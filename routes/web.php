<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/cat', function () {
//     // Artisan::call('migrate');
//     return view('front-end.pages.sigleview.blade');
// });



Route::get('/', function () {
    // Artisan::call('migrate');
    return view('front-end.coming-soon.index');
})->name('index');

Route::get('/contact', function () {
    // Artisan::call('migrate');
    return view('front-end.pages.contact');
})->name('contact');
Route::get('post-publish', 'Admin\PostPublishController@postpublish');


Route::get('/config-cache',function(){
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    Artisan::call('queue:restart');
});

Route::get('/file',function(){
    Artisan::call('queue:work',['--stop-when-empty' => true]);


});

Route::get('/mig', function () {
    Artisan::call('migrate');
    // return 'success';
    // return view('front-end.pages.sigleview.blade');
});

Route::get('/test', function () {
    

    for ($i=0; $i <count(pageTokens()) ; $i++) { 
        // \App\Jobs\SendPost::dispatch(pageTokens()[$i]);

        Toolkito\Larasap\SendTo::Facebook(
            'link',
            [
                'link' =>'http://topcini.com',
                'message' => ' Added at Topcini',
                'page_access_token' => pageTokens()[$i]
            ]
        );
        

    }

    // \App\Jobs\SendPost::dispatch();
});

// Route::get('config-cache',function(){
//     Artisan::call('config:cache');
//     Artisan::call('view:clear');
//     return 'success';
// });


// Auth::routes();


$this->get('aurait', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('aurait', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');


Route::name('password.edit')->get('password-change', 'Admin\UserController@passwordReset');
Route::name('password.update')->post('password-change', 'Admin\UserController@postPasswordReset');

Route::name('password.editadmin')->get('/password-change', 'Admin\UserController@passwordResetadmin');
Route::name('password.update')->post('/password-change', 'Admin\UserController@postPasswordReset');

Route::name('password.request')->get('/password-request', 'Admin\UserController@passwordChange');
Route::name('password.request')->post('/password-request', 'Admin\UserController@postPasswordChange');

Route::group(['prefix'=>'admin','middleware' => ['auth']], function(){
    Route::resource('category', 'Admin\CategoryController');
    Route::resource('comments', 'Admin\CommentsController');
    Route::resource('subscribers', 'Admin\SubscriberController');
    Route::resource('contacts', 'Admin\ContactController');
    Route::resource('post', 'Admin\PostController');
    Route::get('search', 'Front\TechFascinoController@backendsearch')->name('search');

    
   
});

Route::group(['middleware' => ['auth']], function(){
   
    Route::resource('permission', 'Admin\PermissionController');
    Route::resource('role', 'Admin\RoleController');
    Route::resource('user', 'Admin\UserController');
});



// Route::get('/page', 'GraphController@publishToPage')->name('fb');

Route::post('child', 'Admin\PostController@child')->name('child');
// ///////////////////////////////////////////////////////////////

//// Route::get('/', 'Front\TechFascinoController@index')->name('index');
Route::post('savecomment', 'Front\TechFascinoController@savecomment')->name('savecomment');
Route::post('subscribe', 'Front\TechFascinoController@subscribe')->name('subscribe');

Route::post('contact', 'Front\TechFascinoController@contact')->name('contactform');

Route::get('searchresults', 'Front\TechFascinoController@searchresults')->name('searchresults');

Route::get('/{slug?}', 'Front\TechFascinoController@categoryview')->name('category');
Route::get('/{categoryslug?}/{postslug?}', 'Front\TechFascinoController@singlepost')->name('singlepost');

// Route::get('/{category?}/{drama?}', 'Front\TopciniController@singledrama')->name('singledrama');

// Route::get('/{category?}/{drama?}/{postdrama?}', 'Front\TopciniController@singleserial')->name('singleserial');

// Route::post('autocomplete', 'Front\TopciniController@autocomplete')->name('autocomplete');

Route::get('test-email', 'Admin\JobController@processQueue');

Route::get('/', 'Front\TechFascinoController@index')->name('index');







