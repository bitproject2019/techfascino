<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Post,Subscribe,Category,Contact,Comment};
use Illuminate\Support\Facades\Validator;



class TechFascinoController extends Controller
{
    public function index()
    {
        $latestposts = Post::latest()->limit(10)->get();
        $technology = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',6);
        })->latest()->limit(6)->get();
        $mobile = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',5);
        })->latest()->limit(6)->get();
        $gadget = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',7);
        })->latest()->limit(6)->get();
        $video = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',10);
        })->latest()->limit(6)->get();
        $science = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',4);
        })->latest()->limit(7)->get();
        $world = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',9);
        })->latest()->limit(6)->get();
        $lifestyle = Post::latest()->whereHas('categories', function ($query) {
            $query->where('category_id',8);
        })->latest()->limit(6)->get();

        $random = Post::inRandomOrder()->limit(5)->get();

        return view('front-end.pages.index',compact('latestposts','technology','mobile','gadget','video','science','world','lifestyle','random'));
    }

    public function subscribe(Request $request){
        $checkmail = Subscribe::whereEmail($request->email)->first();
        if(!isset($checkmail)){
            $subscribe = new Subscribe();
            $subscribe->name = $request->name;
            $subscribe->email = $request->email;
            $subscribe->save();
            return response()->json([
                'success' => true,
                'message'=>'Thanks for subscribing!!!!',
            ]);
        }
        else
        {
            return response()->json([
               
                'message'=>'You are Already Subscribed!!!!',
            ]);
        }
        
    }

    public function contact(Request $request){
      
        // return $request;
        $validation = validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);
        if($validation->passes())
        {
         
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->subject = $request->subject;
            $contact->message = $request->message;
            $contact->status = 0;
            $contact->save();
            return response()->json([
                'success' => true,
                'message'=>'Thanks for contacting us we will be reply soon!!!!',
            ]);
        }
        else
        {
            return response()->json([

                'message'=>'Sorry all the fields are required!!!',
            ]);
        }   
       
        
    }

    public function categoryview($slug)
    {
        $category_id = Category::whereSlug($slug)->pluck('id');

        $posts = Post::latest()->whereHas('categories', function ($query) use($category_id) {
            $query->whereIn('category_id',$category_id);
        })->paginate(12);
        
        return view('front-end.pages.category',compact('posts','slug'));
    }
    public function singlepost($category_slug,$post_slug)
    {
        // return $post_slug;

        $post = Post::whereSlug($post_slug)->first();
        $previous = Post::where('id', '<', $post->id)->orderBy('id','desc')->first();
        $next = Post::where('id', '>', $post->id)->orderBy('id')->first();
        $category_id = Category::whereSlug($category_slug)->pluck('id');

        $relatedposts = Post::whereHas('categories', function ($query) use($category_id) {
            $query->where('category_id',$category_id);
        })->latest()->limit(6)->get();
        $comments = Comment::wherePostId($post->id)->whereStatus('Verified')->get();
        return view('front-end.pages.singleview',compact('category_slug','post','previous','next','relatedposts','comments'));
    }

    public function savecomment(Request $request)
    {
        $validation = validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required',
            'post_id' => 'required',
            'message' => 'required',
        ]);
        if($validation->passes())
        {
         
            $comment = new Comment();
            $comment->name = $request->name;
            $comment->email = $request->email;
            $comment->post_id = $request->post_id;
            $comment->comment = $request->message;
            $comment->status = 'Pending';
            $comment->save();
            return response()->json([
                'success' => true,
                'message'=>'Thanks for commenting!!!!',
            ]);
        }
        else
        {
            return response()->json([

                'message'=>'Sorry all the fields are required!!!',
            ]);
        }   
    }

    public function searchresults()
    {
        $search =  request()->search;
        $posts = Post::latest()->where('title', 'LIKE', '%' . $search . '%')->paginate(12);
        // return $posts;
        return view('front-end.pages.searchview',compact('posts'));
    }
   
    public function backendsearch(Request $request)
    {
        $sea = request()->search;
        $posts = Post::where('title', 'LIKE', "%{$sea}%")->get();
        return view('back-end.post.index',compact('posts'));
    }


   
}
