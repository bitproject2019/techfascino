<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Category,Post};

use Illuminate\Support\Facades\Validator;
use File;
use Toolkito\Larasap\SendTo;
use Illuminate\Support\Facades\Queue;
use App\Jobs\SendPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use App\Authorizable;


class PostController extends Controller
{
    use Authorizable;

    // use Queueable , SerializesModels;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $posts = Post::all();
        return view('back-end.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereParentId(null)->with('children')->get();
        return view('back-end.post.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  return url('/');
        // $image = Category::find($request->category_id);
        // $image->image;
        $validation = validator::make($request->all(),[
            'image' =>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'title' => 'required',
            'category_id' => 'required',

            
        ]);
        
        if($validation->passes())
        {
            $slug = str_slug($request->title);
            $post = new Post();
            $post->title = $request->title;
            $post->slug = isset($request->slug)?str_replace([' ',':','--','&'],'-',$request->slug):$slug;
            $post->content = $request->content;
            $post->video = isset($request->video)? $request->video:null;

            // $post->category_id = $request->category_id;
            if($request->hasfile('image'))
            {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $image->move(public_path('post_image'),$name);    
                $post->image = $name;
            }
            $post->save();
            // $post->slug = $slug;
            // return $post;
            $post->categories()->sync($request->category_id);

            $category = Category::find($request->category_id[0]);

            // return '/front-end/images/news/lifestyle/food1.jpg';
            SendTo::Facebook(
                'link',
                [
                    'link' => url('/').'/'.$category->name.'/'.$post->slug,
                    'message' => $post->title.' Added At Tech Fascino',
                    'page_access_token' => 'EAALeEDzopyYBALWCIK6RZAV1RXMnMW7O65HZBZB4fK6nNnjZB9L2DHhZBZCfbLTdUBSgMDKjZCqIh9M3q7Sr6NagaFMqNJFsMZCmqRXT8jMO8QEZAOyYwfvPV1ZANcvhQpcH7wkjiAdM2PqjnACKbTViZCciBniXkqQpSGUaFuNMTZA7eSCbLLYl1dmCDLlfkeKxEVAZD'
                ]
            );

            SendTo::Twitter(
                $post->title.' Post Added at Techfascino' .' '.url('/').'/'.$category->name.'/'.$post->slug ,
                [
                    public_path('post_image/'.$post->image),
                    
                ]
            );
         
       
            

            return redirect()->back();
        }
        else
        {
            return redirect()->back()->withErrors($validation)
            ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $categories = Category::whereParentId(null)->with('children')->get();

        return view('back-end.post.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $validation = validator::make($request->all(),[
            // 'image' =>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'title' => 'required',
            'category_id' => 'required',
        ]);
        if($validation->passes())
        {
         
            $post = Post::find($id);
            $post->title = $request->title;
            $post->slug = isset($request->slug)?str_replace([' ',':','--','&'],'-',$request->slug):str_slug($request->title);
            $post->content = $request->content;
            $post->video = isset($request->video)? $request->video:null;
            if($request->hasfile('image'))
            {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $image->move(public_path('post_image'),$name);    
                $post->image = $name;
            }

            $post->save();
            $post->categories()->sync($request->category_id);
            
            return redirect()->back();
        }
        else
        {
            return redirect()->back()->withErrors($validation)
            ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $image_path = "post_image/".$post->image;

        if(File::exists($image_path)) {
            File::delete($image_path);

        }
        $post->delete();
    

        return response()->json([
            'success'=>true,
            'message'=>'Deleted Successful',
        ]);
    }

    public function child(Request $request)
    {
        $child_categories = Category::whereParentId($request->id)->pluck('name','id');
        return  response()->json([
            'child' => $child_categories

        ]);
    }
}
