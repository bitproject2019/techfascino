<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\{Category,Post,Comment,Contact};

class AppServiceProvider extends ServiceProvider
{
    
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__.'/../helper.php';

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('front-end.shared.trending',function($view){
         
            $latestposts = Post::latest()->limit(5)->get();
            $view->with(['latestposts'=>$latestposts]);

        });
        
        view()->composer('front-end.shared.side-bar',function($view){
         
            $randomposts = Post::inRandomOrder()->limit(6)->get();
            $categories = Category::whereParentId(null)->get();
            $latestposts = Post::latest()->limit(6)->get();
            $view->with(['randomposts'=>$randomposts,'categories'=>$categories,'latestposts'=>$latestposts]);

        });
        view()->composer('front-end.shared.footer',function($view){
         
            $randomposts = Post::inRandomOrder()->limit(3)->get();
            $categories = Category::whereParentId(null)->get();
            $latestposts = Post::latest()->limit(3)->get();
            $view->with(['randomposts'=>$randomposts,'categories'=>$categories,'latestposts'=>$latestposts]);

        });

        view()->composer('back-end.shared.header',function($view){
         
            $pendingcomments = Comment::whereStatus('Pending')->get();
            $contacts = Contact::whereStatus('0')->get();
            $view->with(['pendingcomments'=>$pendingcomments,'contacts'=>$contacts]);

        });

        view()->composer('front-end.pages.category',function($view){
         
            $randomposts = Post::inRandomOrder()->limit(6)->get();
            $categories = Category::whereParentId(null)->get();
            $latestposts = Post::latest()->limit(6)->get();
            $view->with(['randomposts'=>$randomposts,'categories'=>$categories,'latestposts'=>$latestposts]);

        });


    }
}
