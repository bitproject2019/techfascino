<?php
use App\Models\District;
use App\User;


function systemDate()
{
    return stoday();
}

function systemVersion()
{
  return '1.0';
}

function user()
{
  return auth()->user();
}

function adminInc($file)
{
    return 'admin.shared.'.$file;
}

function uniqueSeries()
{
    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));
    $series = substr(csrf_token(),0,10).$d->format("YmdHisu");
    return $series;
}

function authUserName()
{
  if (!auth()->guest()) {
    return auth()->user()->username;
  }
  return 'Guest';
}



function authLoginSecure($i)
{
  Auth::logout();
  Auth::login(User::find($i));
}

function systemValues($type='title',$index = '')
{


   $values = [
     'title'=>[
       'Mr'=>'Mr',
       'Miss'=>'Miss',
       'Mrs'=>'Mrs',
       'Master'=>'Master',
       'Baby'=>'Baby',
       'Sis'=>'Sister',
       'Fa'=>'Father',
       'Dr'=>'Dr',
       'Honerable'=>'Honerable'
     ],

     'status'=>[
      'Pending'=>'Pending',
      'Verified'=>'Verified'
     
    ],
    'contact'=>[
      '0'=>'Unread',
      '1'=>'Read'
     
    ],
     
   ];
   if (empty($index)) {
     return $values[$type];
   }
   return $values[$type][$index];
}

function defaultImage()
{
    return '/admin/images/default.jpg';
}


function empty_val($value='',$r='-')
{
  if (empty($value)) {
    return $r;
  }
  return $value;
}

function checkHtml($status = true)
{
  if ($status) {
    return '<span class="label label-info"><i class="fa fa-check"></i></span>';
  }
  return '<span class="label label-default"><i class="fa fa-times"></i></span>';
}




function userID()
{
  if (!auth()->guest()) {
    return auth()->user()->id;
  }
  return 0;
}

function isActiveRoute($route,$output='active')
 {      
    $resorce = [$route.'.index',$route.'.create',$route.'.edit',$route.'.show'];

    if (($pos = strpos($route, ".")) !== FALSE) { 
        $resorce = [$route];
    }
    $current = Route::currentRouteName();

    if (in_array($current,$resorce)) {
         return $output;
    }
 }

 function isActiveUrl($route,$add_str='',$output='active')
 {      
    $url = route($route).$add_str;
    $current = request()->fullUrl();

    if( $current== $url){
      return $output;
    }
    return '';

 }

//  function money_format($amount)
//  {
//    return number_format($amount,2);
//  }



function toDecimal($value='0.00',$d='0.00')
{
  // if ($value <='0') {
  //   return $d;
  // }
  return (float) str_replace(',', "", $value);
}


function extract_numbers($string,$stat = true)
{
   $numbers = preg_replace('/[^0-9]/', '', $string);
   $letters = preg_replace('/[^a-zA-Z]/', '', $string);
   if ($stat) {
      return $numbers;
   }
   return $letters;#
}

function time_since($datetime,$full = false) {
    if (date('Y',strtotime($datetime)) > date('Y')) {
      return 'Unknown';
    }
    $now = new DateTime;
    $ago = new DateTime($datetime);

    $diff = $now->diff($ago);
    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'min',
        's' => 'sec',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }
    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


 function dateFormat($date,$f='d M, Y')
 {
   return date($f,strtotime($date));
 }

 
function strDates($range)
{
    return  date('Y-m-d',strtotime($range));
}


function annual($r='start')
{
  $d = [
      'start'=>date('Y-01-01'),
      'end'=>date('Y-12-31')
  ];
  return $d[$r];
}

function all($r='start')
{
  $d = [
      'start'=>'2010-01-01',
      'end'=>date('Y-m-d')
  ];
  return $d[$r];
}

function todayR($r='start')
{
  $d = [
      'start'=>stoday(),
      'end'=>stoday()
  ];
  return $d[$r];
}

function stoday()
{
    return  strDates('today');
}

function tomorrow()
{
    return  strDates('tomorrow');
}

function thisMonth($r='start')
{
    $d = [
        'start'=>strDates('first day of this month'),
        'end'=>stoday()
    ];
    return $d[$r];
}

function lastMonth($r='start')
{
    $d = [
        'start'=>strDates('first day of last month'),
        'end'=>strDates('last day of last month')
    ];
    return $d[$r];
}

function thisWeek($r='start')
{
    $d = [
        'start'=>strDates('monday this week'),
        'end'=>strDates('sunday this week')
    ];
    return $d[$r];
}

function lastWeek($r='start')
{
    $d = [
        'start'=>strDates('last week monday'),
        'end'=>strDates('last week sunday')
    ];
    return $d[$r];
}


function rangeMonth($datestr,$r='start') {
    date_default_timezone_set(date_default_timezone_get());
    $dt = strtotime($datestr);
    $res['start'] = date('Y-m-d', strtotime('first day of this month', $dt));
    $res['end'] = date('Y-m-d', strtotime('last day of this month', $dt));
    return $res[$r];
}

function formatTime($time='',$format= 'H:i:s')
{
  if (empty($time)) $time = date('H:i:s');
  return date($format,strtotime($time));
  
}
function formatDate($date='',$format= 'd, F Y')
{
  return date($format,strtotime($date));
  
}


function convert_number_to_words($number) {

    $hyphen      = '';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Fixteen',
        17                  => 'Feventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function get_greeting_message()
{
  $Hour = date('G');
  if ( $Hour >= 5 && $Hour <= 11 ) {
      $text =  "Good Morning";
  } else if ( $Hour >= 12 && $Hour <= 18 ) {
      $text =  "Good Afternoon";
  } else if ( $Hour >= 19 || $Hour <= 4 ) {
      $text =  "Good Evening";
  }
  $user_name = auth()->user()->name;

  return 'Welcome to HMS! '.$text. ' '.$user_name;
}


function pageTokens()
{
  return ['EAAXe52J9wC0BAJ5fXwhZCweMrZC2uNuy7t3pfTx1lDayxeUxcNEB6Cye4rPU13hSXcKv3xPLiPTiHfuwSA4xhZCpVJRImzRCZA81n7imZCuuiZA55aCI9UJXOYIRh2NZCxeheSD3U5xO4ntoyigNHKYKVoi4Erx4dzOsPRloG0QUhQyqD3sPAzckdYvjzfZChPEZD','EAAXe52J9wC0BABPugoYj5iqRccaaQ2FM5rhy01pamuo1cdF7uU5hCjOwWFvpgxXZCUZCLrNAeGFQAlZAVB1kFljBT6OldMaApu3WHNuQBPgiZCooLoGNZBgEn9g9M8EFVLywCIzZAx5kyoofgwynL9yhxqhOfNjZA3Xxsae1zpZBn2porSJ7WaCtYfJkEopGxNEZD','EAAXe52J9wC0BAPyQJkCEVFohDBWrp3gQR8NEfkMrx9WPjUP4c9RHpFea2zhgf9nT21Jka9V3B48GpS4rJQMDRInhj5LxVF01SpoFmBSBfpCKe6MdDiZCGuboyRdMZC8jqZCdHnYIRw6jwRrOjeL8p6E6M4BzTqBr60wXPTm1XoWMdaCN1KvbL0HWwzZBbZAgZD','EAAXe52J9wC0BAGaIyd9POZBVRJIM8ginpPLsq16ixUfuCHcPTxEED0R6LiufyZAejOAcUzwcMZBteCXRY0jgzroQctsomNfwbr0IN47yO3ulZCzYfZAgJZChmDx4ZBzZCHqUw75fdqh9Fsr5jZC4ZCZASUZCXFcRnkgnvFjLiOLlIvXTnbYZC793UetuUzp9ZAJQhzcC4ZD','EAAXe52J9wC0BANPRH73YgIz2l6lHCyB08yF9b68CKxl1cRy8UWA0OXDe7VRZBmAMLU0PWmXEG4qWwphX4EbA3zqH8L5udW4qtYcEsGhNVpZC8Trcqjpad3ZBr53GJ3laYcWfcmuQJrYk6QRAURSA3jGsdm1mrVCaEFVBcAsH9AqViRqO1l0SRQzoZAccbBIZD','EAAXe52J9wC0BAHllpTxPALV0KpFXEMlhexyBeosZA2W4jY6Qk9JlQ5ZBJRFdJ4QPKDD2nR7ebJZChMkOgKSJ3LhA4TqD7B2MdX4ZA5hfdZCPeeIllB3IrnfvLk6IbHAkRaW4Qr5oDeAVWmZCvAFShJarY4iowiYTB6U7mZCFmKkOCykWVkJPfqtaOCdMuKlHXkZD','EAAXe52J9wC0BAFGraCSWYllZAGsE5ACTuXISBCyAWlPRmShc5DSZBPda21qJRuKLYYywhEvYMajOFX5XJi6jy7simNz8G0k8HbR4i2tWIuooqyZBwgwnMyWcQZBCuJTgI3wPj0FcPV3elUQr4inMxd0Uo36L0HZBuQRMF1QXKqWXQZBZAno1ZAVvGXIhVZA474fAZD','EAAXe52J9wC0BAOqtxGaJLOV3GNN2dQ9RCjBpmDiwdZA2377RRhtk23co2MKY7jz40s0hdSMIQHvE4ZCW6PSEsCjxkEmQ3abt5NyM6tl29tRkB0fyZBuuDY7Dg2ivLveQnOI9Xw6VdxZC8J6e02QGKZBudZBkwUSW9Dq507VcXvrOARC39ZAFMmZAhrqZCGTA7ZCJEZD','EAAXe52J9wC0BAGNnzJlzjQ8ysRia2vmBRZCZCJh35ns2IHyPZAhEmWqqAIkZAyiIQga9WdOi86KIC2ZCS0yo7BzZAFjpsHUiNGoZBMZCa6hZAtVmKEzZBZBnDtbrBQZAP4fdzQmLc0FmeRPFT0bAuM2xXYZBrNWwHUzc9ZCpjVx9BZAA39nl346GlXPTECGjJCQjhTyAlcZD','EAAXe52J9wC0BAMlMA0zb7apGQaYveuoI9w5i7hyOkwZApolFGZCFAAiqposTcM6KGvez3XBcb8aVw8zWpKDJ409PvAWPRPmPHg0x6sX30IUUT87ILr1QC0cZAAojrPw7A5ZBO5q2q2O1hndpkZAKTUZAdOnPAqwlBIZBQDBQYNcMkN9NGJtagf7iYi9CBZBzq0kZD','EAAXe52J9wC0BAPxRsYOG2SflK2eHRdNF4bDtZCiUlZAflPXJwqLHyIzN9pJaHC7wwtd35SDEF4eHCBSJ6ZBOiUxAZCrnpzkbZC9kUHbgJDU7jJjGcMYBpyYLVNbaN6NzfnAsETP1iddlFgZBBebE0GYFXmgrMVuC5MRheW4yMK8txr3hiX2nFzvjZBBG0F1QNcZD','EAAXe52J9wC0BACCr1hHzgtz9zglSBTMxhYtqJFEC0lTgc9EBPkDuXWYq9Nj6l8VyDI0jsRX36gukvLtABlHuSVRJFYobf9HdHiy67eIf6koIpqobzNQuxYJaZAVwNZAyPGiiMR51I3NOhxQspXmayLaB6eDVeeqNEUkdd6eTRm35ZBvOd7fbHm8TAc2btsZD','EAAXe52J9wC0BAPmZBAd95qYh7JlAQBNj6r4tGwuwRVcJmMsmzE3bhZC7GVJAvpr4KWvTvarKYiCfSL8ZBYuwun6j3ECWqivD7KbF9ObPQdNcfSt9UGeTO9PGOHjhBLycApt014Bvt9bSyqOqNDk8oNwKLXheuerCQQsprHtItFmK6DLt5mxVhC2ZCZBnchAUZD','EAAXe52J9wC0BAEFa7xiHg8SUqYqG8aZBItLSLT39s85m36hPapt408oz41pWu8jS8zqU4vdG1qJNZA4b2fsHZCsD1UPyM4REFK5wGxYS46FDWuhZCEZAdvpPOpZAo5GNuwu8cMQYJJzX5LyU7s9105ij1R6HalQq4uYGtHOgblST1MxecEZBTFyhViJ4UGap8wZD','EAAXe52J9wC0BAPOCUAhkyu28b1LRwpTagAMUjCY7PIl4YQWSyd9Klkk3yyd5UO0YLfCBZAbeNJtZBZBMFXKhoYJN2Q0B3xZBUROzuE5OjIilRNUwPKfpZC3DHtDinnmZCWBcPxt9EuR3KcAk7eHQnGPMyg9rJrtnu3gADoexSyxv3a4U8wC9SCfpGiEiWrq1IZD','EAAXe52J9wC0BAMZCETx8siJZA6TwuARZAZCXjyHIOmXLMZAKBu3nRWqvHKFKxcUlz4LLyWZBUMCGlb2UdJvjKp0KWihycWlV8CmZChfO0ZArOTNWQ0d72ZBd0LvtrDtRLeRa4YNcX4C2DMs1SgXbVoIJo84rbsjmnIhLG0ZBgNEiCCiEpOM5Ua54iOp5QwkEgBVFsZD','EAAXe52J9wC0BAH4ll0xKF71D26x5iup8aD2FMNiereM8dBdatPXeOXLUXjYxbB9RwPNOnMfb1FE6SDxnCQ0qmGIYaY3rIw0RLEurBbmQ529vZBw3jjUhQyOOyq7mTNn4ZBwKpFxKsEkM5xEwqDINJBqsUun4v73AbaP6OYQZA4AkqoX1iJay65KjwSvNTwZD','EAAXe52J9wC0BAJcQFVZCoa6S7QcfgktkKSZA4qY4kstsAGUZCsZANpV9ivRKu2ZApFdgG3GtS7zk8jHlbZADJVzWz58sYpw9hJdHNMduh2BI1UY0gCyIq7kLEXBfZBV8xRgBPdZA6BXEwaoY36SR4ZCiR8IF8ZAiCnxleCFENOpygos4ofSZBaFrbM07dJuG5ZAhlZBUZD','EAAXe52J9wC0BALuFunyDZBRmHEPPMTQRmy9iH1bDgdkPcQg8TZBqtEDyZBla7P2VqI3ELmV6KRHJyIG0ScXHafkCqNbb68pWMMecnjah8cGqwjb46thZCCwdTusODEiZB3bng7dXLQSeEjcwxFZACmFSGhloBAD2uj7MBiycSDuZBPz7oP56PZCXASNBH0ONmzEZD','EAAXe52J9wC0BALEZCDk5PdJbKbOeI2EcIDZAfXC2drLzSsEPhIlbgEXZBP6xWaozAqjJAdO7Kx8aofMKzYH7LFuVz1InZClIxDLn9EwEFBRTyJc7qZCpsRp76sRqe4u2TlNG5pUe7pocyTCt0wZCHhdva6fD1eNP2hZCuSLEGjWzKtZCRJp4s5EJMV7jcovuCNEZD','EAAXe52J9wC0BAGbIttGNcoX2NS8uvPdNGYJthMkUwpeUeZAfhUV9jaUvZCgZCpZAk1AhcUxicf6QV9ioEEZBZAMzwQoZB5jzURkhy5FipEc6JJ4RZCOg6qFVnhsZBHbaHaz8RNGmT48ZCEreNL0RQrYHBmKwcgYpNOkA4T4b67FZCtBApZBqjYTqwoWOvvHkuoovz3MZD','EAAXe52J9wC0BAN8hFP1KajKORNb7DcovwU5WqZAJ6ZCqiw4bGvLUykPdLMZAvW4SmCLUUrjfL2qNebzh4uDeYQxZAGO39i0gZAQZCytKU3I9bmSiVao9Jx5HYvCZA7jhrsj2ZAURMwNCll36DZAviBnL5g4hVTou9c2SEg6nJbGylAbqjUuQu4K9mZB0Q1g8Et084ZD','EAAXe52J9wC0BAO3yBqLsXVX9LLZBkXn9ZBTtNERZBPH3EJ3Wv5tQgDybp9UKsdtKrMpkMpiEeqbFoeDbg8bG5JwhGTBGuCzZCuf8IU5VMGFNCXJHMw0xuoQGMGYewhdZCCOqj0PnfdDvIPELJVPnFATINnCETOY1msdBxasNP4GZCOa5PesKmJ6APZBzwEh6O8ZD','EAAXe52J9wC0BAAZCKKS06BOYRSx0tEtwgeAowLUCW7VZBgtX5jbMrdwhqyAAVqj9fkAwFACAtufd3yMVWXS81a1hguRO9MNpo8QDrA5YzRBYHWy6pxIZAuDXym0kgZAFELrKRZA2TrUn5HTRWyWyoaLSK16PZASKjjBUBTbMZCoXcacqOeryxzvPEOFNkGPBxAZD','EAAXe52J9wC0BAHtgcuaofB5QA5S0ZBJy7tF3vjmszoU62N6NoZCeSf7neCuTtJYhYQZAlEfhikq6BlSMw3VkVn4BnG0k4j6Y63qVJ5NZANMZCj30VEgfWGvHqQfUXOXZBAfiS4ZA74c1ZAK96y7BlT56wMxMtWFfGKEkyv1ZB1JZAjbasC90uyb941FMMKWBwZA42YZD','EAAXe52J9wC0BAJz8EJln6U34PvbOltP1bxiODtVdAiw22T7wsizheIpYSVUCkmroqO2imu3matYwC9ayemgs5pkT9CQQeDZCXQnaOkNfQH4RXUgy2HJK2BNkZCbYlHv62hoNy972C2QfbLWeaWBbZCfIifDZCqdAqQU28IayOEF1HKKN2oUOWzBi4lrKqsgZD','EAAXe52J9wC0BABU3sDR5sBpb5OaYjEBBXZC8VyzuEJWRt3NuM35WZAZAFXkZADTtnZABZBdDNUvIaERZC9getjhcecc6SfURNNCZABWJyBOIKIIdN1tDrJ8a04PWRlGy1ZBEeKtUcsZBOMRq0zn8ONW5OczRhhSk3ZBAQ5djdLy0Qp0ZBoaBRDLDlBU0C517oRkcfkMZD','EAAXe52J9wC0BAAR9mjW02o0v9boIlaTGQB9zkHK9TWWubhYUDQz0NZCO2r7bC9rrRx3ggqBqoDI20I0KoyJzXOXaTrn6hWl6anGA9s0ZB4QsnS2UfXWEOgN4cbZC5155MyIZAr3Hgyx6iiVz9cpJVbmPI1PSVP7TZAKYptu9AoZAdvattsioZB8tA7HHBrl17QZD','EAAXe52J9wC0BAIshBCKCgLiYWCjxJ9Wr3pI8ZBPIftQPem55ZBNEp9qbik2tTnKdrxgM3r4mR5wnOLxdvdEtlkEshlQIWHJpwL6sWZAZAUn4XmbNsOCwKHkqe6fsBdpDMuolwqJWsAbpKcNOiL1ZAzm5zOQFQz1OmUjJeRZBoosjIHbEibLKjctBIec82sX90ZD'];
}