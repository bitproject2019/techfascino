@extends('front-end.template')
@section('title','Post')
@section('content')

	<div class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ol class="breadcrumb">
     					<li><a href="{{ route('index') }}">Home</a></li>
     					<li><a href="{{ route('category',$category_slug) }}">{{ $category_slug }}</a></li>
     				</ol>
				</div><!-- Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</div><!-- Page title end -->

	<section class="block-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					
					<div class="single-post">
						
						<div class="post-title-area">
              @foreach ($post->categories as $category)

							  <a class="post-cat" href="{{ route('category',$category->name) }}">{{ $category->name }}</a>
                  
              @endforeach
							<h2 class="post-title">
				 				{{ $post->title }}
				 			</h2>
				 			<div class="post-meta">
								<span class="post-author">
									By <a href="#">Admin</a>
								</span>
								<span class="post-date"><i class="fa fa-clock-o"></i> {{ $post->created_at->toFormattedDateString() }}</span>
								{{-- <span class="post-hits"><i class="fa fa-eye"></i> 21</span>
								<span class="post-comment"><i class="fa fa-comments-o"></i>
								<a href="#" class="comments-link"><span>01</span></a></span> --}}
							</div>
						</div><!-- Post title end -->

						<div class="post-content-area">
							<div class="post-media post-featured-image">
								<a href="{{ isset($post->image)?'/post_image/'.$post->image:'/category_image/'.$post->categories[0]->image }}" class="gallery-popup"><img src="{{ isset($post->image)?'/post_image/'.$post->image:'/category_image/'.$post->categories[0]->image }}" class="img-fluid" alt=""></a>
							</div>
							<div class="entry-content">
									@if($post->categories[0]->name == "Video")
									
										<iframe src="{!!  html_entity_decode($post->video) !!}" height="450px" width="100%"></iframe>

									@endif

									@if(isset($post->content))

										<p>{!!  html_entity_decode($post->content) !!}</p>
									@endif

									

							</div><!-- Entery content end -->

							{{-- <div class="tags-area clearfix">
								<div class="post-tags">
									<span>Tags:</span>
		   						<a href="#"># Food</a>
		   						<a href="#"># Lifestyle</a>
		   						<a href="#"># Travel</a>
	   						</div>
							</div><!-- Tags end --> --}}

							<div class="share-items clearfix">
                  <div class="addthis_inline_share_toolbox"></div>
   						</div><!-- Share items end -->

						</div><!-- post-content end -->
					</div><!-- Single post end -->

					<nav class="post-navigation clearfix">
            <div class="post-previous">
              @if (isset($previous))                
                <a href="{{ route('singlepost',[$previous->categories[0]->slug,$previous->slug]) }}">
                    <span><i class="fa fa-angle-left"></i>Previous Post</span>
                    <h3>
                        {{ \Illuminate\Support\Str::words($previous->title,5) }}
                    </h3>
                </a>
              @endif
            </div>
            <div class="post-next">
              @if (isset($next))                
                <a href="{{ route('singlepost',[$next->categories[0]->slug,$next->slug]) }}">
                    <span>Next Post <i class="fa fa-angle-right"></i></span>
                    <h3>
                        {{ \Illuminate\Support\Str::words($next->title,5) }}
                    </h3>
                </a>
              @endif
            </div>       
          </nav><!-- Post navigation end -->

					{{-- <div class="author-box">
						<div class="author-img pull-left">
							<img src="images/news/author.png" alt="">
						</div>
						<div class="author-info">
							<h3>Razon Khan</h3>
							<p class="author-url"><a href="#">http://www.newsdaily247.com</a></p>
							<p>Selfies labore, leggings cupidatat sunt taxidermy umami fanny pack typewriter hoodie art party voluptate. Listicle meditation paleo, drinking vinegar sint direct trade.</p>
							<div class="authors-social">
                        <span>Follow Me: </span>
                        <a href="#"><i class="fa fa-behance"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                    </div>
						</div>
					</div> <!-- Author box end --> --}}

					<div class="related-posts block">
						<h3 class="block-title"><span>Related Posts</span></h3>

						<div id="latest-news-slide" class="owl-carousel owl-theme latest-news-slide">
              
              @foreach ($relatedposts as $relatedpost)
							<div class="item">
								<div class="post-block-style clearfix">
									<div class="post-thumb">
										<a href="{{ route('singlepost',[$category_slug,$relatedpost->slug]) }}"><img class="img-fluid" src="{{ isset($relatedpost->image)?'/post_image/'.$relatedpost->image:'/category_image/'.$relatedpost->categories[0]->image }}" alt="" /></a>
									</div>
									<a class="post-cat" href="{{ route('category',$category_slug) }}">{{ $category_slug }}</a>
									<div class="post-content">
							 			<h2 class="post-title title-medium">
							 				<a href="{{ route('singlepost',[$category_slug,$relatedpost->slug]) }}">{{ \Illuminate\Support\Str::words($relatedpost->title,3) }}</a>
							 			</h2>
							 			<div class="post-meta">
							 				<span class="post-author"><a href="#">Admin</a></span>
							 				<span class="post-date">{{ $relatedpost->created_at->toFormattedDateString() }}</span>
							 			</div>
						 			</div><!-- Post content end -->
								</div><!-- Post Block style end -->
							</div><!-- Item 1 end -->
              @endforeach

						</div><!-- Carousel end -->

					</div><!-- Related posts end -->

					<!-- Post comment start -->
					<div id="comments" class="comments-area block">
						<h3 class="block-title"><span>{{ $comments->count() }} Comments</span></h3>

						<ul class="comments-list">
							<li>
                @foreach ($comments as $comment)
                    
								<div class="comment">
									<img class="comment-avatar pull-left" alt="" src="/front-end/images/user.png">
									<div class="comment-body">
										<div class="meta-data">
											<span class="comment-author">{{ $comment->name }}</span>
											<span class="comment-date pull-right">{{ $comment->created_at->toFormattedDateString() }} at {{ date('h:i a', strtotime($comment->created_at->format('H:i:s'))) }}</span>
										</div>
										<div class="comment-content">
										<p>{{ $comment->comment }}</p></div>
											
									</div>
								</div><!-- Comments end -->
                @endforeach

							
							</li><!-- Comments-list li end -->
						</ul><!-- Comments-list ul end -->
					</div><!-- Post comment end -->

					<div class="comments-form">
						<h3 class="title-normal">Leave a comment</h3>

						<form role="form" id="comment">
              @csrf
							<div class="row">
                <input type="hidden" name="post_id" id="post_id" value="{{ $post->id }}">
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control required-field" id="message" placeholder="Your Comment" rows="10"  name="message"></textarea>
									</div>
								</div><!-- Col end -->

								<div class="col-md-12">
									<div class="form-group">
										<input class="form-control" name="name" id="name" placeholder="Your Name" type="text" >
									</div>
								</div><!-- Col end -->

								<div class="col-md-12">
									<div class="form-group">
										<input class="form-control" name="email" id="email" placeholder="Your Email" type="email" >
									</div>
								</div>

								
							</div><!-- Form row end -->
							<div class="clearfix">
								<button class="comments-btn btn btn-primary" type="submit">Post Comment</button> 
							</div>
            </form><!-- Form end -->
           
					</div><!-- Comments form end -->
          		<div id="saved" class="comments-area block"></div>
			</div><!-- Content Col end -->

        @include('front-end.shared.side-bar') 

			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- First block end -->


	
@stop
@push('script')
<script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
  <script>
    
    $('#comment').submit(function (e) { 
      e.preventDefault();
      var form = new FormData(this);
      $.ajax({
        url:"{{ route('savecomment') }}",
        type:"POST",
        data: form,
        dataType:"Json",
        cache: false,
        contentType: false,
        processData: false
      })  
      .done(function (res) {
        console.log(res);
        if(res.success){
          $('#saved').text(res.message).css('color','green');
        }
        else{
          $('#saved').text(res.message).css('color','red');
        }
        
      })
    });
   
    
  </script>  
@endpush