@extends('front-end.template')
@section('title','Search Results')
@section('content')


	
	<div class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ol class="breadcrumb">
     					<li><a href="{{ route('index') }}">Home</a></li>
     					<li>Search</li>
     				</ol>
				</div><!-- Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</div><!-- Page title end -->

	<section class="block-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">

					<div class="block category-listing category-style2">
						<h3 class="block-title"><span>Searched Results</span></h3>

						@foreach ($posts as $post)				
							<div class="post-block-style post-list clearfix">
								<div class="row">
									<div class="col-lg-5 col-md-6">
										<div class="post-thumb thumb-float-style">
											<a href="#">
												<img class="img-fluid" src="{{ isset($post->image)?'/post_image/'.$post->image:'/category_image/'.$post->categories[0]->image }}" alt="" />
											</a>
											<a class="post-cat" href="{{ route('category',$post->categories[0]->slug) }}">{{ $post->categories[0]->slug }}</a>
										</div>
									</div><!-- Img thumb col end -->

									<div class="col-lg-7 col-md-6">
										<div class="post-content">
											<h2 class="post-title title-large">
												<a href="{{ route('singlepost',[$post->categories[0]->slug,$post->slug]) }}">{{ $post->title }}</a>
											</h2>
											<div class="post-meta">
												<span class="post-author"><a href="#">Admin</a></span>
												<span class="post-date">{{ $post->created_at->toFormattedDateString() }}</span>
												{{-- <span class="post-comment pull-right"><i class="fa fa-comments-o"></i> --}}
												{{-- <a href="#" class="comments-link"><span>03</span></a></span> --}}
											</div>
											<p>{!!  html_entity_decode(\Illuminate\Support\Str::words($post->content,30)) !!}</p>
										</div><!-- Post content end -->
									</div><!-- Post col end -->
								</div><!-- 1st row end -->
							</div><!-- 1st Post list end -->
						@endforeach
						

					</div><!-- Block Technology end -->

					<div class="paging">
		            <ul class="pagination">
					  <li>{{ $posts->links() }}</li>
		              <li>
		              	<span class="page-numbers">Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}</span>
		              </li>
		            </ul>
	          	</div>


				</div><!-- Content Col end -->

				@include('front-end.shared.side-bar') 

			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- First block end -->



@stop