@extends('front-end.template')
@section('title','Index')
@section('content')

<section class="featured-post-area no-padding">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-12 pad-r">
        <div id="featured-slider" class="owl-carousel owl-theme featured-slider content-bottom">
          @foreach ($latestposts as $latestpost)
            <div class="item" style="background-image:url({{ isset($latestpost->image) ? asset('/post_image') . '/'.$latestpost->image : asset('/category_image') . '/'.$latestpost->categories[0]->image}})">
              <div class="featured-post">
                <div class="post-content">
                 
                    @foreach ($latestpost->categories as $category)
                      
                    <a class="post-cat" href="{{ route('category',$category->slug) }}">{{ $category->name }} </a>
                        
                    @endforeach
                    
                  
                  <h2 class="post-title title-extra-large">
                    <a href="{{ route('singlepost',[$latestpost->categories[0]->slug,$latestpost->slug]) }}">{{ $latestpost->title }}</a>
                  </h2>
                  <span class="post-date">{{ $latestpost->created_at->toFormattedDateString() }}</span>
                </div>
              </div><!--/ Featured post end -->
              
            </div><!-- Item 1 end -->  
          @endforeach

         
        </div><!-- Featured owl carousel end-->
      </div><!-- Col 7 end -->

      <div class="col-lg-5 col-md-12 pad-l">
        <div class="row">
          <div class="col-md-12">
            <div class="post-overaly-style contentBottom hot-post-top clearfix">
              <div class="post-thumb">
                <a href="{{ route('singlepost',[$latestposts[3]->categories[0]->slug,$latestposts[3]->slug]) }}">
                  <img class="img-fluid" src="{{ isset($latestposts[3]->image)?'/post_image/'.$latestposts[3]->image:'/category_image/'.$latestposts[3]->categories[0]->image }}" alt="" />
                </a>
              </div>
              <div class="post-content">
                  @foreach ($latestposts[3]->categories as $category)   
                    <a class="post-cat" href="{{ route('category',$category->slug) }}">{{ $category->name }} </a>   
                  @endforeach
                <h2 class="post-title title-large">
                  <a href="{{ route('singlepost',[$latestposts[3]->categories[0]->slug,$latestposts[3]->slug]) }}">{{  \Illuminate\Support\Str::words($latestposts[3]->title,2) }}</a>
                </h2>
                <span class="post-date">{{ $latestposts[3]->created_at->toFormattedDateString() }}</span>
              </div><!-- Post content end -->
            </div><!-- Post Overaly end -->
          </div><!-- Col end -->

          <div class="col-md-6 pad-r-small">
            <div class="post-overaly-style contentBottom hot-post-bottom clearfix">
              <div class="post-thumb">
                <a href="{{ route('singlepost',[$latestposts[4]->categories[0]->slug,$latestposts[4]->slug]) }}"> 
                  <img class="img-fluid" src="{{ isset($latestposts[4]->image)?'/post_image/'.$latestposts[4]->image:'/category_image/'.$latestposts[4]->categories[0]->image }}" alt="" />
                </a>
              </div>
              <div class="post-content">
                  @foreach ($latestposts[4]->categories as $category)   
                    <a class="post-cat" href="#">{{ $category->name }} </a>   
                  @endforeach
                <h2 class="post-title title-medium">
                  <a href="{{ route('singlepost',[$latestposts[4]->categories[0]->slug,$latestposts[4]->slug]) }}">{{ \Illuminate\Support\Str::words($latestposts[4]->title,2) }}</a>
                </h2>
                <span class="post-date">{{ $latestposts[4]->created_at->toFormattedDateString() }}</span>
              </div><!-- Post content end -->
            </div><!-- Post Overaly end -->
          </div><!-- Col end -->

          <div class="col-md-6 pad-l-small">
            <div class="post-overaly-style contentBottom hot-post-bottom clearfix">
              <div class="post-thumb">
                <a href="{{ route('singlepost',[$latestposts[5]->categories[0]->slug,$latestposts[5]->slug]) }}">
                  <img class="img-fluid" src="{{ isset($latestposts[5]->image)?'/post_image/'.$latestposts[5]->image:'/category_image/'.$latestposts[5]->categories[0]->image }}" alt="" />
                </a>
              </div>
              <div class="post-content">
               
                @foreach ($latestposts[5]->categories as $category)   
                  <a class="post-cat" href="{{ route('category',$category->slug) }}">{{ $category->name }} </a>   
                @endforeach
             
                <h2 class="post-title title-medium">
                  <a href="{{ route('singlepost',[$latestposts[5]->categories[0]->slug,$latestposts[5]->slug]) }}">{{ \Illuminate\Support\Str::words($latestposts[5]->title,2) }}</a>
                </h2>
                <span class="post-date">{{ $latestposts[5]->created_at->toFormattedDateString() }}</span>
              </div><!-- Post content end -->
            </div><!-- Post Overaly end -->
          </div><!-- Col end -->
        </div>
      </div><!-- Col 5 end -->

    </div><!-- Row end -->
  </div><!-- Container end -->
</section><!-- Trending post end -->

<section class="block-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12">
        <!--- Featured Tab startet -->

        <!--- Technology Tab startet -->
        <div class="featured-tab color-orange">
          <h3 class="block-title"><span> <a href="{{ route('category','Technology') }}" >Technology</a></span></h3>
         
          <div class="tab-content">
              <div class="tab-pane animated fadeInRight active" id="tab_a">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                      <div class="post-block-style clearfix">
                          <div class="post-thumb">
                            <a href="{{ route('singlepost',['Technology',$technology[0]->slug]) }}">
                                <img class="img-fluid" src="{{ isset($technology[0]->image)?'/post_image/'.$technology[0]->image:'/category_image/'.$technology[0]->categories[0]->image }}" alt="" />
                            </a>
                          </div>
                        
                          {{-- <a class="post-cat" href="#">Gadgets</a> --}}
                          <div class="post-content">
                                <h2 class="post-title">
                                  <a href="{{ route('singlepost',['Technology',$technology[0]->slug]) }}">
                                    {{ \Illuminate\Support\Str::words($technology[0]->title,5) }}
                                  </a>
                                </h2>
                                <div class="post-meta">
                                  <span class="post-author"><a href="#">Admin</a></span>
                                  <span class="post-date">{{ $technology[0]->created_at->toFormattedDateString() }}</span>
                                </div>
                                <p>{!! html_entity_decode(\Illuminate\Support\Str::words($technology[0]->content,35)) !!}</p>
                               
                            </div><!-- Post content end -->
                      </div><!-- Post Block style end -->
                    </div><!-- Col end -->

                    <div class="col-lg-6 col-md-6">
                      <div class="list-post-block">
                          <ul class="list-post">
                            @for ($i = 1; $i <=4; $i++)
                                
                                <li class="clearfix">
                                  <div class="post-block-style post-float clearfix">
                                    <div class="post-thumb">
                                        <a href="{{ route('singlepost',['Technology',$technology[$i]->slug]) }}">
                                          <img class="img-fluid" src="{{ isset($technology[$i]->image)?'/post_image/'.$technology[$i]->image:'/category_image/'.$technology[$i]->categories[0]->image }}" alt="" />
                                        </a>
                                    </div><!-- Post thumb end -->
                                    {{-- <a class="post-cat" href="#">{{ $technology[$i]->categories[0]->name }}</a> --}}

                                    <div class="post-content">
                                          <h2 class="post-title title-small">
                                              <a href="{{ route('singlepost',['Technology',$technology[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($technology[$i]->title,5) }}</a>
                                          </h2>
                                          <div class="post-meta">
                                              <span class="post-date">{{ $technology[$i]->created_at->toFormattedDateString() }}</span>
                                          </div>
                                        </div><!-- Post content end -->
                                  </div><!-- Post block style end -->
                              </li><!-- Li 1 end -->
                            @endfor
                           
                          </ul><!-- List post end -->
                      </div><!-- List post block end -->
                    </div><!-- List post Col end -->
                </div><!-- Tab pane Row 1 end -->
              </div><!-- Tab pane 1 end -->
              <div class="tab-pane animated fadeInRight" id="tab_b">
                <div class="row">
                <div class="col-lg-6 col-md-6">
                   
                </div><!-- Col end -->

              </div><!-- Tab pane Row 2 end -->
              </div><!-- Tab pane 2 end -->
          </div><!-- tab content -->
        </div>
        <!-- Technology Tab end -->

        <div class="gap-20"></div>

        <!--- Mobile Tab startet -->
        <div class="block">
          <div class="row">

            <div class="col-lg-6 col-md-6 color-violet">
              <h3 class="block-title"><span>Mobile</span></h3>
              
              <div class="post-overaly-style clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Mobile',$mobile[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($mobile[0]->image)?'/post_image/'.$mobile[0]->image:'/category_image/'.$mobile[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Mobile',$mobile[0]->slug]) }}">{{ \Illuminate\Support\Str::words($mobile[0]->title,5) }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-date">{{ $mobile[0]->created_at->toFormattedDateString() }}</span>
                  </div>
                </div><!-- Post content end -->
              </div><!-- Post Overaly Article end -->

              <div class="list-post-block">
                <ul class="list-post">
                  @for ($i = 1; $i <= 4; $i++)
                    <li class="clearfix">
                      <div class="post-block-style post-float clearfix">
                        <div class="post-thumb">
                          <a href="{{ route('singlepost',['Mobile',$mobile[$i]->slug]) }}">
                            <img class="img-fluid" src="{{ isset($mobile[$i]->image)?'/post_image/'.$mobile[$i]->image:'/category_image/'.$mobile[$i]->categories[0]->image }}" alt="" />
                          </a>
                        </div><!-- Post thumb end -->

                        <div class="post-content">
                          <h2 class="post-title title-small">
                            <a href="{{ route('singlepost',['Mobile',$mobile[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($mobile[$i]->title,5) }} </a>
                          </h2>
                          <div class="post-meta">
                            <span class="post-date">{{ $mobile[$i]->created_at->toFormattedDateString() }}</span>
                          </div>
                        </div><!-- Post content end -->
                      </div><!-- Post block style end -->
                    </li><!-- Li 1 end -->
                  @endfor
                 

                </ul><!-- List post end -->
              </div><!-- List post block end -->
            </div><!-- Col 1 end -->

            <div class="col-lg-6 col-md-6 color-aqua">
                <h3 class="block-title"><span>Gadget</span></h3>
                <div class="post-overaly-style clearfix">
                  <div class="post-thumb">
                    <a href="{{ route('singlepost',['Gadget',$gadget[0]->slug]) }}">
                      <img class="img-fluid" src="{{ isset($gadget[0]->image)?'/post_image/'.$gadget[0]->image:'/category_image/'.$gadget[0]->categories[0]->image }}" alt="" />
                    </a>
                  </div>
                  
                  <div class="post-content">
                    <h2 class="post-title">
                      <a href="{{ route('singlepost',['Gadget',$gadget[0]->slug]) }}">{{ \Illuminate\Support\Str::words($gadget[0]->title,5) }}</a>
                    </h2>
                    <div class="post-meta">
                      <span class="post-date">{{ $gadget[0]->created_at->toFormattedDateString() }}</span>
                    </div>
                  </div><!-- Post content end -->
                </div><!-- Post Overaly Article end -->
  
                <div class="list-post-block">
                  <ul class="list-post">
                    @for ($i = 1; $i <= 4; $i++)
                      <li class="clearfix">
                        <div class="post-block-style post-float clearfix">
                          <div class="post-thumb">
                            <a href="{{ route('singlepost',['Gadget',$gadget[$i]->slug]) }}">
                              <img class="img-fluid" src="{{ isset($gadget[$i]->image)?'/post_image/'.$gadget[$i]->image:'/category_image/'.$gadget[$i]->categories[0]->image }}" alt="" />
                            </a>
                          </div><!-- Post thumb end -->
  
                          <div class="post-content">
                            <h2 class="post-title title-small">
                              <a href="{{ route('singlepost',['Gadget',$gadget[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($gadget[$i]->title,5) }} </a>
                            </h2>
                            <div class="post-meta">
                              <span class="post-date">{{ $gadget[$i]->created_at->toFormattedDateString() }}</span>
                            </div>
                          </div><!-- Post content end -->
                        </div><!-- Post block style end -->
                      </li><!-- Li 1 end -->
                    @endfor
                   
  
                  </ul><!-- List post end -->
                </div><!-- List post block end -->
              </div><!-- Col 1 end -->
          </div><!-- Row end -->
        </div>
        <!-- Block mobile end -->

        <div class="gap-40"></div>

        <!--- Science Tab startet -->
        <div class="featured-tab color-red">
            <h3 class="block-title"><span>Science</span></h3>
            
  
            <div class="tab-content">
                <div class="tab-pane animated fadeInRight active" id="tab_a">
                  <div class="row">
                      <div class="col-lg-6 col-md-6">
                        <div class="post-block-style clearfix">
                            <div class="post-thumb">
                              <a href="{{ route('singlepost',['Science',$science[0]->slug]) }}">
                                  <img class="img-fluid" src="{{ isset($science[0]->image)?'/post_image/'.$science[0]->image:'/category_image/'.$science[0]->categories[0]->image }}" alt="" />
                              </a>
                            </div>
                          
                            {{-- <a class="post-cat" href="#">Gadgets</a> --}}
                            <div class="post-content">
                                  <h2 class="post-title">
                                    <a href="{{ route('singlepost',['Science',$science[0]->slug]) }}">
                                      {{ \Illuminate\Support\Str::words($science[0]->title,5) }}
                                    </a>
                                  </h2>
                                  <div class="post-meta">
                                    <span class="post-author"><a href="#">Admin</a></span>
                                    <span class="post-date">{{ $science[0]->created_at->toFormattedDateString() }}</span>
                                  </div>
                                  <p>{!! html_entity_decode(\Illuminate\Support\Str::words($science[0]->content,35)) !!}</p>
                                 
                              </div><!-- Post content end -->
                        </div><!-- Post Block style end -->
                      </div><!-- Col end -->
  
                      <div class="col-lg-6 col-md-6">
                        <div class="list-post-block">
                            <ul class="list-post">
                              @for ($i = 1; $i <=4; $i++)
                                  
                                  <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                      <div class="post-thumb">
                                          <a href="{{ route('singlepost',['Science',$science[$i]->slug]) }}">
                                            <img class="img-fluid" src="{{ isset($science[$i]->image)?'/post_image/'.$science[$i]->image:'/category_image/'.$science[$i]->categories[0]->image }}" alt="" />
                                          </a>
                                      </div><!-- Post thumb end -->
 
                                      <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="{{ route('singlepost',['Science',$science[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($science[$i]->title,5) }}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">{{ $science[$i]->created_at->toFormattedDateString() }}</span>
                                            </div>
                                          </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 1 end -->
                              @endfor
                             
                            </ul><!-- List post end -->
                        </div><!-- List post block end -->
                      </div><!-- List post Col end -->
                  </div><!-- Tab pane Row 1 end -->
                </div><!-- Tab pane 1 end -->
                <div class="tab-pane animated fadeInRight" id="tab_b">
                  <div class="row">
                  <div class="col-lg-6 col-md-6">
                      
                  </div><!-- Col end -->
  
                </div><!-- Tab pane Row 2 end -->
                </div><!-- Tab pane 2 end -->
            </div><!-- tab content -->
          </div>
          <!-- science Tab end -->

        <div class="gap-20"></div>

              
        <!--- World Tab startet -->
        <div class="featured-tab color-blue">
            <h3 class="block-title"><span>World</span></h3>
            
  
            <div class="tab-content">
                <div class="tab-pane animated fadeInRight active" id="tab_a">
                  <div class="row">
                      <div class="col-lg-6 col-md-6">
                        <div class="post-block-style clearfix">
                            <div class="post-thumb">
                              <a href="{{ route('singlepost',['World',$world[0]->slug]) }}">
                                  <img class="img-fluid" src="{{ isset($world[0]->image)?'/post_image/'.$world[0]->image:'/category_image/'.$world[0]->categories[0]->image }}" alt="" />
                              </a>
                            </div>
                          
                            {{-- <a class="post-cat" href="#">Gadgets</a> --}}
                            <div class="post-content">
                                  <h2 class="post-title">
                                    <a href="{{ route('singlepost',['World',$world[0]->slug]) }}">
                                      {{ \Illuminate\Support\Str::words($world[0]->title,5) }}
                                    </a>
                                  </h2>
                                  <div class="post-meta">
                                    <span class="post-author"><a href="#">Admin</a></span>
                                    <span class="post-date">{{ $world[0]->created_at->toFormattedDateString() }}</span>
                                  </div>
                                  <p>{!! html_entity_decode(\Illuminate\Support\Str::words($world[0]->content,35)) !!}</p>
                                 
                              </div><!-- Post content end -->
                        </div><!-- Post Block style end -->
                      </div><!-- Col end -->
  
                      <div class="col-lg-6 col-md-6">
                        <div class="list-post-block">
                            <ul class="list-post">
                              @for ($i = 1; $i <=4; $i++)
                                  
                                  <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                      <div class="post-thumb">
                                          <a href="{{ route('singlepost',['World',$world[$i]->slug]) }}">
                                            <img class="img-fluid" src="{{ isset($world[$i]->image)?'/post_image/'.$world[$i]->image:'/category_image/'.$world[$i]->categories[0]->image }}" alt="" />
                                          </a>
                                      </div><!-- Post thumb end -->
 
                                      <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="{{ route('singlepost',['World',$world[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($world[$i]->title,5) }}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">{{ $world[$i]->created_at->toFormattedDateString() }}</span>
                                            </div>
                                          </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 1 end -->
                              @endfor
                             
                            </ul><!-- List post end -->
                        </div><!-- List post block end -->
                      </div><!-- List post Col end -->
                  </div><!-- Tab pane Row 1 end -->
                </div><!-- Tab pane 1 end -->
                <div class="tab-pane animated fadeInRight" id="tab_b">
                  <div class="row">
                  <div class="col-lg-6 col-md-6">
                      
                  </div><!-- Col end -->
  
                </div><!-- Tab pane Row 2 end -->
                </div><!-- Tab pane 2 end -->
            </div><!-- tab content -->
          </div>
          <!-- science Tab end -->

        <div class="gap-20"></div>
        <!-- World Tab end -->

        


        <!--- Lifestyle Tab startet -->
        <div class="featured-tab color-aqua">
            <h3 class="block-title"><span>Lifestyle</span></h3>
            
  
            <div class="tab-content">
                <div class="tab-pane animated fadeInRight active" id="tab_a">
                  <div class="row">
                      <div class="col-lg-6 col-md-6">
                        <div class="post-block-style clearfix">
                            <div class="post-thumb">
                              <a href="{{ route('singlepost',['Lifestyle',$lifestyle[0]->slug]) }}">
                                  <img class="img-fluid" src="{{ isset($lifestyle[0]->image)?'/post_image/'.$lifestyle[0]->image:'/category_image/'.$lifestyle[0]->categories[0]->image }}" alt="" />
                              </a>
                            </div>
                          
                            {{-- <a class="post-cat" href="#">Gadgets</a> --}}
                            <div class="post-content">
                                  <h2 class="post-title">
                                    <a href="{{ route('singlepost',['Lifestyle',$lifestyle[0]->slug]) }}">
                                      {{ \Illuminate\Support\Str::words($lifestyle[0]->title,5) }}
                                    </a>
                                  </h2>
                                  <div class="post-meta">
                                    <span class="post-author"><a href="#">Admin</a></span>
                                    <span class="post-date">{{ $lifestyle[0]->created_at->toFormattedDateString() }}</span>
                                  </div>
                                  <p>{!! html_entity_decode(\Illuminate\Support\Str::words($lifestyle[0]->content,35)) !!}</p>
                                 
                              </div><!-- Post content end -->
                        </div><!-- Post Block style end -->
                      </div><!-- Col end -->
  
                      <div class="col-lg-6 col-md-6">
                        <div class="list-post-block">
                            <ul class="list-post">
                              @for ($i = 1; $i <=4; $i++)
                                  
                                  <li class="clearfix">
                                    <div class="post-block-style post-float clearfix">
                                      <div class="post-thumb">
                                          <a href="{{ route('singlepost',['Lifestyle',$lifestyle[$i]->slug]) }}">
                                            <img class="img-fluid" src="{{ isset($lifestyle[$i]->image)?'/post_image/'.$lifestyle[$i]->image:'/category_image/'.$lifestyle[$i]->categories[0]->image }}" alt="" />
                                          </a>
                                      </div><!-- Post thumb end -->
 
                                      <div class="post-content">
                                            <h2 class="post-title title-small">
                                                <a href="{{ route('singlepost',['Lifestyle',$lifestyle[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($lifestyle[$i]->title,5) }}</a>
                                            </h2>
                                            <div class="post-meta">
                                                <span class="post-date">{{ $lifestyle[$i]->created_at->toFormattedDateString() }}</span>
                                            </div>
                                          </div><!-- Post content end -->
                                    </div><!-- Post block style end -->
                                </li><!-- Li 1 end -->
                              @endfor
                             
                            </ul><!-- List post end -->
                        </div><!-- List post block end -->
                      </div><!-- List post Col end -->
                  </div><!-- Tab pane Row 1 end -->
                </div><!-- Tab pane 1 end -->
                <div class="tab-pane animated fadeInRight" id="tab_b">
                  <div class="row">
                  <div class="col-lg-6 col-md-6">
                      
                  </div><!-- Col end -->
  
                </div><!-- Tab pane Row 2 end -->
                </div><!-- Tab pane 2 end -->
            </div><!-- tab content -->
          </div>
          <!-- science Tab end -->

        <div class="gap-20"></div>
        <!-- Lifestyle Tab end -->

       

      </div><!-- Content Col end -->

      <!--- Sidebar Col started -->
      <div class="col-lg-4 col-md-12">
        <div class="sidebar sidebar-right">
          <div class="widget">
            <h3 class="block-title"><span>Follow Us</span></h3>

            <ul class="social-icon">
              <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
              <li><a href="https://www.facebook.com/techfascino/" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
              <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
          </div><!-- Widget Social end -->

          <div class="widget color-default">
            <h3 class="block-title"><span>Latest Post</span></h3>

            <div class="post-overaly-style clearfix">
              <div class="post-thumb">
                <a href="{{ route('singlepost',[$latestposts[0]->categories[0]->slug ,$latestposts[0]->slug]) }}">
                  <img class="img-fluid" src="{{ isset($latestposts[0]->image)?'/post_image/'.$latestposts[0]->image:'/category_image/'.$latestposts[0]->categories[0]->image }}" alt="" />
                </a>
              </div>
              
              <div class="post-content">
                <a class="post-cat" href="{{ route('category',$latestposts[0]->categories[0]->slug) }}">{{ $latestposts[0]->categories[0]->name }}</a>
                <h2 class="post-title">
                  <a href="{{ route('singlepost',[$latestposts[0]->categories[0]->slug ,$latestposts[0]->slug]) }}">{{ \Illuminate\Support\Str::words($latestposts[0]->title,5) }}</a>
                </h2>
                <div class="post-meta">
                  <span class="post-date">{{ $latestposts[0]->created_at->toFormattedDateString() }}</span>
                </div>
              </div><!-- Post content end -->
            </div><!-- Post Overaly Article end -->


            <div class="list-post-block">
              <ul class="list-post">
                @for ($i = 1; $i < 5; $i++)

                  <li class="clearfix">
                    <div class="post-block-style post-float clearfix">
                      <div class="post-thumb">
                        <a href="{{ route('singlepost',[$latestposts[$i]->categories[0]->slug ,$latestposts[$i]->slug]) }}">
                          <img class="img-fluid" src="{{ isset($latestposts[$i]->image)?'/post_image/'.$latestposts[$i]->image:'/category_image/'.$latestposts[$i]->categories[0]->image }}" alt="" />
                        </a>
                        <a class="post-cat" href="{{ route('category',$latestposts[$i]->categories[0]->slug) }}">{{ $latestposts[$i]->categories[0]->name }}</a>
                      </div><!-- Post thumb end -->

                      <div class="post-content">
                        <h2 class="post-title title-small">
                          <a href="{{ route('singlepost',[$latestposts[$i]->categories[0]->slug ,$latestposts[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($latestposts[$i]->title,5) }}</a>
                        </h2>
                        <div class="post-meta">
                          <span class="post-date">{{ $latestposts[$i]->created_at->toFormattedDateString() }}</span>
                        </div>
                      </div><!-- Post content end -->
                    </div><!-- Post block style end -->
                  </li><!-- Li 1 end -->
                @endfor
               

              </ul><!-- List post end -->
            </div><!-- List post block end -->

          </div><!-- Popular news widget end -->

          <div class="widget text-center">
            <img class="banner img-fluid" src="/front-end/images/banner-ads/ad-sidebar.png" alt="" />
                </div><!-- Sidebar Ad end -->
                
        </div><!-- Sidebar right end -->
      </div><!-- Sidebar Col end -->

    </div><!-- Row end -->
  </div><!-- Container end -->
  </section><!-- First block end -->

 <!-- Video block start --> 
<section class="block-wrapper video-block color-green">
  <div class="container">
    <div class="row">
      <div class="video-tab clearfix">
              <h3 class="block-title"><span>Videos</span></h3>
        <div class="row">
        <div class="col-lg-7 pad-r-0">
          <div class="tab-content">
            <div class="tab-pane active animated fadeIn" id="video0">
              <div class="post-overaly-style clearfix">
                  <div class="post-thumb">
                  <img class="img-fluid" src="{{ isset($video[0]->image)?'/post_image/'.$video[0]->image:'/category_image/'.$video[0]->categories[0]->image }}" alt="" />
                  <a class="popup" href="{{$video[0]->video  }}">
                          <div class="video-icon">
                            <i class="fa fa-play"></i>
                        </div>
                      </a>
                  </div><!-- Post thumb end -->
                  <div class="post-content">
                    <a class="post-cat" href="{{ route('category','video') }}">Video</a>
                    <h2 class="post-title">
                        <a href="{{ route('singlepost',['video',$video[0]->slug]) }}">{{ $video[0]->title }}</a>
                    </h2>
                  </div><!-- Post content end -->
              </div><!-- Post Overaly Article end -->
            </div><!--Tab pane 1 end-->
            @for ($i = 1; $i <= 4; $i++)
              <div class="tab-pane animated fadeIn" id="video{{ $i }}">
                <div class="post-overaly-style clearfix">
                    <div class="post-thumb">
                      <img class="img-fluid" src="{{ isset($video[$i]->image)?'/post_image/'.$video[$i]->image:'/category_image/'.$video[$i]->categories[0]->image }}" alt="" />
                    <a class="popup" href="{{$video[$i]->video  }}">
                            <div class="video-icon">
                              <i class="fa fa-play"></i>
                          </div>
                        </a>
                    </div><!-- Post thumb end -->
                    <div class="post-content">
                      <a class="post-cat" href="{{ route('category','video') }}">Video</a>
                      <h2 class="post-title title-medium">
                          <a href="{{ route('singlepost',['video',$video[$i]->slug]) }}">{{ $video[$i]->title }}</a>
                      </h2>
                    </div><!-- Post content end -->
                </div><!-- Post Overaly Article 2 end -->
              </div><!--Tab pane 2 end-->
            @endfor
            
            

          </div><!-- Tab content end -->
        </div><!--Tab col end -->

        <div class="col-lg-5 pad-l-0">
          <ul class="nav nav-tabs">
              <li class="nav-item active">
                <a class="nav-link animated fadeIn" href="#video0" data-toggle="tab">
                  <div class="post-thumb">
                      <img class="img-fluid" src="{{ isset($video[0]->image)?'/post_image/'.$video[0]->image:'/category_image/'.$video[0]->categories[0]->image }}" alt="" />
                  </div><!-- Post thumb end -->
                  <h3>{{ $video[0]->title }}</h3>
                </a>
              </li>
              @for ($i = 1; $i < 3; $i++)
              <li class="nav-item">
                <a class="nav-link animated fadeIn" href="#video{{ $i }}" data-toggle="tab">
                  <div class="post-thumb">
                      <img class="img-fluid" src="{{ isset($video[$i]->image)?'/post_image/'.$video[$i]->image:'/category_image/'.$video[$i]->categories[0]->image }}" alt="" />
                  </div><!-- Post thumb end -->
                  <h3>{{ $video[$i]->title }}</h3>
                </a>
              </li>
            @endfor
          </ul>
        </div><!--Tab nav col end -->

        </div>
      </div><!-- Video tab end -->

    </div><!-- Row end -->
  </div><!-- Container end -->
</section>
<!-- Video block end -->

<!-- more news block started -->
<section class="block-wrapper p-bottom-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12">
        <div class="more-news block color-red">
          <h3 class="block-title"><span>More News</span></h3>

          <div id="more-news-slide" class="owl-carousel owl-theme more-news-slide">
            <div class="item">
              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Technology',$technology[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($technology[0]->image)?'/post_image/'.$technology[0]->image:'/category_image/'.$technology[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Technology') }}">Technology</a>               
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Technology',$technology[0]->slug]) }}">{{ $technology[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $technology[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($technology[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 1 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Mobile',$mobile[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($mobile[0]->image)?'/post_image/'.$mobile[0]->image:'/category_image/'.$mobile[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Mobile') }}">Mobile</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Mobile',$mobile[0]->slug]) }}">{{ $mobile[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $mobile[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($mobile[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 2 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Gadget',$gadget[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($gadget[0]->image)?'/post_image/'.$gadget[0]->image:'/category_image/'.$gadget[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Gadget') }}">Gadget</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Gadget',$gadget[0]->slug]) }}">{{ $gadget[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $gadget[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($gadget[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 3 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Science',$science[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($science[0]->image)?'/post_image/'.$science[0]->image:'/category_image/'.$science[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Science') }}">Science</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Science',$science[0]->slug]) }}">{{ $science[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $science[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($science[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 4 end -->
            </div><!-- Item 1 end -->

            <div class="item">
              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['World',$world[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($world[0]->image)?'/post_image/'.$world[0]->image:'/category_image/'.$world[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','World') }}">World</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['World',$world[0]->slug]) }}">{{ $world[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $world[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($world[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 5 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Lifestyle',$lifestyle[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($lifestyle[0]->image)?'/post_image/'.$lifestyle[0]->image:'/category_image/'.$lifestyle[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Lifestyle') }}">Lifestyle</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Lifestyle',$lifestyle[0]->slug]) }}">{{ $lifestyle[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $lifestyle[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($lifestyle[0]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 6 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['video',$video[0]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($video[0]->image)?'/post_image/'.$video[0]->image:'/category_image/'.$video[0]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','video') }}">Video</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['video',$video[0]->slug]) }}">{{ $video[0]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $video[0]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p> <a href="{!!  html_entity_decode(\Illuminate\Support\Str::words($video[0]->content,35)) !!}">Watch Video</a></p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 7 end -->

              <div class="gap-30"></div>

              <div class="post-block-style post-float-half clearfix">
                <div class="post-thumb">
                  <a href="{{ route('singlepost',['Science',$science[5]->slug]) }}">
                    <img class="img-fluid" src="{{ isset($science[5]->image)?'/post_image/'.$science[5]->image:'/category_image/'.$science[5]->categories[0]->image }}" alt="" />
                  </a>
                </div>
                <a class="post-cat" href="{{ route('category','Science') }}">Science</a>
                <div class="post-content">
                  <h2 class="post-title">
                    <a href="{{ route('singlepost',['Science',$science[5]->slug]) }}">{{ $science[5]->title }}</a>
                  </h2>
                  <div class="post-meta">
                    <span class="post-author"><a href="#">Admin</a></span>
                    <span class="post-date">{{ $science[5]->created_at->toFormattedDateString()  }}</span>
                  </div>
                  <p>{!!  html_entity_decode(\Illuminate\Support\Str::words($science[5]->content,35)) !!}</p>
                </div><!-- Post content end -->
              </div><!-- Post Block style 8 end -->
            </div><!-- Item 2 end -->
            
          </div><!-- More news carousel end -->
        </div><!--More news block end -->
      </div><!-- Content Col end -->

      <div class="col-lg-4 col-sm-12">
        <div class="sidebar sidebar-right">

                <div class="widget color-default">
                    <h3 class="block-title"><span>Trending News</span></h3>
  
                    <div id="post-slide" class="owl-carousel owl-theme post-slide">
                      <div class="item">
                          @for ($i = 0; $i < 2; $i++)
                            <div class="post-overaly-style text-center clearfix">
                              <div class="post-thumb">
                                  <a href="{{ route('singlepost',[$random[$i]->categories[0]->slug,$random[$i]->slug]) }}">
                                    <img class="img-fluid" src="{{ isset($random[$i]->image)?'/post_image/'.$random[$i]->image:'/category_image/'.$random[$i]->categories[0]->image }}" alt="" />
                                  </a>
                              </div><!-- Post thumb end -->

                              <div class="post-content">
                                  <a class="post-cat" href="{{ route('category',$random[$i]->categories[0]->slug) }}">{{ $random[$i]->categories[0]->name }}</a>
                                  <h2 class="post-title">
                                    <a href="{{ route('singlepost',[$random[$i]->categories[0]->slug,$random[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($random[$i]->title,5)}}</a>
                                  </h2>
                                  <div class="post-meta">
                                    <span class="post-date">{{ $random[$i]->created_at->toFormattedDateString()  }}</span>
                                  </div>
                              </div><!-- Post content end -->
                            </div><!-- Post Overaly Article 1 end -->
                          @endfor
                         

                      </div><!-- Item 1 end -->

                      <div class="item">
                         @for ($i = 2; $i < 4; $i++)
                            <div class="post-overaly-style text-center clearfix">
                              <div class="post-thumb">
                                  <a href="{{ route('singlepost',[$random[$i]->categories[0]->slug,$random[$i]->slug]) }}">
                                    <img class="img-fluid" src="{{ isset($random[$i]->image)?'/post_image/'.$random[$i]->image:'/category_image/'.$random[$i]->categories[0]->image }}" alt="" />
                                  </a>
                              </div><!-- Post thumb end -->

                              <div class="post-content">
                                  <a class="post-cat" href="{{ route('category',$random[$i]->categories[0]->slug) }}">{{ $random[$i]->categories[0]->name }}</a>
                                  <h2 class="post-title">
                                    <a href="{{ route('singlepost',[$random[$i]->categories[0]->slug,$random[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($random[$i]->title,5)}}</a>
                                  </h2>
                                  <div class="post-meta">
                                    <span class="post-date">{{ $random[$i]->created_at->toFormattedDateString()  }}</span>
                                  </div>
                              </div><!-- Post content end -->
                            </div><!-- Post Overaly Article 1 end -->
                          @endfor
                      </div><!-- Item 2 end -->

                    </div><!-- Post slide carousel end -->
  
                </div><!-- Trending news end -->

                <div class="widget m-bottom-0">
                    <h3 class="block-title"><span>Newsletter</span></h3>
                    <div class="ts-newsletter">
                      <div class="newsletter-introtext">
                          <h4>Get Updates</h4>
                          <p>Subscribe our newsletter to get the best stories into your inbox!</p>
                      </div>

                      <div class="newsletter-form">
                          <form action="#" method="post" id="subscribe">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg" placeholder="E-mail" autocomplete="off">
                                <button class="btn btn-primary">Subscribe</button>

                            </div>
                          </form>
                          <div id="subres"></div>
                      </div>
                    </div><!-- Newsletter end -->
                </div>
                
        </div><!--Sidebar right end -->
      </div><!-- Sidebar col end -->
    </div><!-- Row end -->
  </div><!-- Container end -->
</section>
<!-- more news block end -->

<!-- Ad content two started -->
<section class="ad-content-area text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid" src="/front-end/images/banner-ads/ad-content-two.png" alt="" width="790px" height="60px" />
      </div><!-- Col end -->
    </div><!-- Row end -->
  </div><!-- Container end -->
</section>
<!-- Ad content two end -->

@push('script')
  <script>
    $('#subscribe').submit(function (e) { 
      e.preventDefault();
      var form = new FormData(this);
      $.ajax({
        url:"{{ route('subscribe') }}",
        type:"POST",
        data: form,
        dataType:"Json",
        cache: false,
        contentType: false,
        processData: false
      })  
      .done(function (res) {
        console.log(res);
        if(res.success){
          $('#subres').text(res.message).css('color','green');
    
        }
        else{
          $('#subres').text(res.message).css('color','red');
        }
        
      })
    });
  </script>
@endpush

@stop