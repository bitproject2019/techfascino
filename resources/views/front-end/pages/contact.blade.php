@extends('front-end.template')
@section('title','Contact')
@section('content')

<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>Contact</li>
                </ol>
            </div><!-- Col end -->
        </div><!-- Row end -->
    </div><!-- Container end -->
</div><!-- Page title end -->
    
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">

            <h3>Contact Us</h3>
            <p>Tech Fascino Worldwide is a popular online newsportal and going source for technical and digital content for its influential audience around the globe. You can get to know the latest news about the all over the world espically about the tech news. You can contact us using the following details </p>

            <div class="widget contact-info">
                <div class="contact-info-box">
                    <div class="contact-info-box-content">
                        <h4>Mail Us</h4>
                        <p>techfascino@gmail.com</p>
                    </div>
                </div>

                <div class="contact-info-box">
                    <div class="contact-info-box-content">
                        <h4>Call Us</h4>
                        <p>+9477-128-9586</p>
                    </div>
                </div>

            </div><!-- Widget end -->
            <br>
            <h3>Contact Form</h3>
                <form id="contactform" >
                    @csrf
                    <div class="error-container"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Name</label>
                            <input class="form-control form-control-name" name="name" id="name" placeholder="" type="text" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control form-control-email" name="email" id="email" 
                                placeholder="" type="email" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Subject</label>
                                <input class="form-control form-control-subject" name="subject" id="subject" 
                                placeholder="" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea class="form-control form-control-message" name="message" id="message" placeholder="" rows="10" ></textarea>
                    </div>
                    <div class="text-right"><br>
                        <button class="btn btn-primary solid blank" type="submit">Send Message</button> 
                    </div>
                </form>

                <div id="contactres"></div>

            </div><!-- Content Col end -->

            @include('front-end.shared.side-bar') 


        </div><!-- Row end -->
    </div><!-- Container end -->
</section><!-- First block end -->

@push('script')
  <script>
    $('#contactform').submit(function (e) { 
      e.preventDefault();
      var form = new FormData(this);
      $.ajax({
        url:"{{ route('contactform') }}",
        type:"POST",
        data: form,
        dataType:"Json",
        cache: false,
        contentType: false,
        processData: false
      })  
      .done(function (res) {
        console.log(res);
        if(res.success){
          $('#contactres').text(res.message).css('color','green');
    
        }
        else{
          $('#contactres').text(res.message).css('color','red');
          $(':input').css('border-color', 'red');
        }
        
      })
    });
  </script>
@endpush

@stop