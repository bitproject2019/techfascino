<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="copyright-info">
                <span>Copyright &copy; {{ date('Y') }} Tech Fascino. All Rights Reserved. Designed By Tech Fascino</span>
            </div>
        </div>

        <div class="col-lg-6 col-md-12">
            <div class="footer-menu">
                <ul class="nav unstyled">
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li><a href="{{ route('contact') }}">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div><!-- Row end -->

    <div id="back-to-top" class="back-to-top">
        <button class="btn btn-primary" title="Back to Top">
            <i class="fa fa-angle-up"></i>
        </button>
    </div>

</div><!-- Container end -->