<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-6">
       <span class="ts-date-top">
          <i class="fa fa-calendar-check-o"> </i>{{ Carbon\Carbon::now()->toFormattedDateString() }}
       </span>
        </div><!--/ Top bar left end -->

        <div class="col-lg-4 col-md-6 ml-auto topbar-info">
       <div class="topbar-user-panel">
          {{-- <span class="ts-login">
                <a href="#"><i class="fa fa-lock"></i></a>
          </span>
          <span class="ts-register active">
             <a href="#"><i class="fa fa-key"></i></a>
          </span> --}}
       </div>
       <div class="topbar-social">
          <ul class="unstyled">
             <li>
                <a title="Facebook" href="https://www.facebook.com/techfascino/">
                   <span class="social-icon"><i class="fa fa-facebook"></i></span>
                </a>
             </li>
             <li>
                <a title="Twitter" href="#">
                   <span class="social-icon"><i class="fa fa-twitter"></i></span>
                </a>
             </li>
             <li>
                <a title="Google+" href="#">
                   <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                </a>
             </li>
             <li>
                <a title="Linkdin" href="#">
                   <span class="social-icon"><i class="fa fa-pinterest"></i></span>
                </a>
             </li>
             <li>
                <a title="Rss" href="#">
                   <span class="social-icon"><i class="fa fa-youtube"></i></span>
                </a>
             </li>
             <li>
                <a title="Skype" href="#">
                   <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                </a>
             </li>
          </ul><!-- Ul end -->
       </div>
        </div><!--/ Top social col end -->
    </div><!--/ Content row end -->
</div><!--/ Container end -->