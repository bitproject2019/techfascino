<div class="col-md-12">
    <h3 class="trending-title">Trending</h3>
    <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
        @foreach ($latestposts as $latestpost)
            <div class="item">
                <div class="post-content">
                    <h2 class="post-title title-small">
                    <a href="{{ route('singlepost',[$latestpost->categories[0]->slug ,$latestpost->slug]) }}">{{ $latestpost->title }}</a>
                    </h2>
                </div><!-- Post content end -->
            </div><!-- Item 1 end -->
        @endforeach
       
    </div><!-- Carousel end -->
</div><!-- Col end -->