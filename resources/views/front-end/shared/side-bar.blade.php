<div class="col-lg-4 col-md-12">
    <div class="sidebar sidebar-right">
        <div class="widget">
            <h3 class="block-title"><span>Follow Us</span></h3>

            <ul class="social-icon">
                <li><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
                <li><a href="https://www.facebook.com/techfascino/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
                <li><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div><!-- Widget Social end -->

        <div class="widget color-default">
            <h3 class="block-title"><span>Popular News</span></h3>

            <div class="post-overaly-style clearfix">
                <div class="post-thumb">
                    <a href="{{ route('singlepost',[$randomposts[0]->categories[0]->slug ,$randomposts[0]->slug]) }}">
                        <img class="img-fluid" src="{{ isset($randomposts[0]->image)?'/post_image/'.$randomposts[0]->image:'/category_image/'.$randomposts[0]->categories[0]->image }}" alt="" />
                    </a>
                </div>
                
                <div class="post-content">
                    <a class="post-cat" href="{{ route('category',$randomposts[0]->categories[0]->slug) }}">{{ $randomposts[0]->categories[0]->name  }}</a>
                    <h2 class="post-title title-small">
                        <a href="{{ route('singlepost',[$randomposts[0]->categories[0]->slug ,$randomposts[0]->slug]) }}">{{ $randomposts[0]->title }}</a>
                    </h2>
                    <div class="post-meta">
                        <span class="post-date">{{ $randomposts[0]->created_at->toFormattedDateString()  }}</span>
                    </div>
                </div><!-- Post content end -->
            </div><!-- Post Overaly Article end -->


            <div class="list-post-block">
                <ul class="list-post">
                    @for ($i = 1; $i < 5; $i++)
                        
                        <li class="clearfix">
                            <div class="post-block-style post-float clearfix">
                                <div class="post-thumb">
                                    <a href="{{ route('singlepost',[$randomposts[$i]->categories[0]->slug ,$randomposts[$i]->slug]) }}">
                                        <img class="img-fluid" src="{{ isset($randomposts[$i]->image)?'/post_image/'.$randomposts[$i]->image:'/category_image/'.$randomposts[$i]->categories[0]->image }}" alt="" />
                                    </a>
                                    <a class="post-cat" href="{{ route('category',$randomposts[$i]->categories[0]->slug) }}">{{ $randomposts[$i]->categories[0]->name  }}</a>
                                </div><!-- Post thumb end -->

                                <div class="post-content">
                                    <h2 class="post-title title-small">
                                        <a href="{{ route('singlepost',[$randomposts[$i]->categories[0]->slug ,$randomposts[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($randomposts[$i]->title,5) }}</a>
                                    </h2>
                                    <div class="post-meta">
                                        <span class="post-date">{{ $randomposts[$i]->created_at->toFormattedDateString()  }}</span>
                                    </div>
                                </div><!-- Post content end -->
                            </div><!-- Post block style end -->
                        </li><!-- Li 1 end -->
                    @endfor

                    

                </ul><!-- List post end -->
            </div><!-- List post block end -->

        </div><!-- Popular news widget end -->

        <div class="widget color-default">
            <h3 class="block-title"><span>Advertisements</span></h3>
            <img class="banner img-fluid" src="{{ asset('front-end/images/banner-ads/ad-sidebar.png') }}" alt="" />
        </div><!-- Sidebar Ad end -->

        <div class="widget m-bottom-0">
            <h3 class="block-title"><span>Newsletter</span></h3>
            <div class="ts-newsletter">
                <div class="newsletter-introtext">
                    <h4>Get Updates</h4>
                    <p>Subscribe our newsletter to get the best stories into your inbox!</p>
                </div>

                <div class="newsletter-form">
                    <form action="#" method="post" id="subscribe">
                        @csrf
                        <div class="form-group">
                            <input type="email" name="email" id="newsletter-form-email" class="form-control form-control-lg" placeholder="E-mail" autocomplete="off">
                            <button class="btn btn-primary">Subscribe</button>
                        </div>
                    </form>
                    <div id="subres"></div>
                </div>
            </div><!-- Newsletter end -->
        </div><!-- Newsletter widget end -->
        
        <!-- latest post widget start -->
        {{-- <div class="widget color-default">
            <h3 class="block-title"><span>Latest Post</span></h3>

            <div class="post-overaly-style clearfix">
                <div class="post-thumb">
                <a href="#">
                    <img class="img-fluid" src="{{ isset($latestposts[0]->image)?'/post_image/'.$latestposts[0]->image:'/category_image/'.$latestposts[0]->categories[0]->image }}" alt="" />
                </a>
                </div>
                
                <div class="post-content">
                <a class="post-cat" href="#">{{ $latestposts[0]->categories[0]->name }}</a>
                <h2 class="post-title">
                    <a href="#">{{ \Illuminate\Support\Str::words($latestposts[0]->title,5) }}</a>
                </h2>
                <div class="post-meta">
                    <span class="post-date">{{ $latestposts[0]->created_at->toFormattedDateString() }}</span>
                </div>
                </div><!-- Post content end -->
            </div><!-- Post Overaly Article end -->


            <div class="list-post-block">
                <ul class="list-post">
                @for ($i = 1; $i < 5; $i++)

                    <li class="clearfix">
                    <div class="post-block-style post-float clearfix">
                        <div class="post-thumb">
                        <a href="#">
                            <img class="img-fluid" src="{{ isset($latestposts[$i]->image)?'/post_image/'.$latestposts[$i]->image:'/category_image/'.$latestposts[$i]->categories[0]->image }}" alt="" />
                        </a>
                        <a class="post-cat" href="#">{{ $latestposts[$i]->categories[0]->name }}</a>
                        </div><!-- Post thumb end -->

                        <div class="post-content">
                        <h2 class="post-title title-small">
                            <a href="#">{{ \Illuminate\Support\Str::words($latestposts[$i]->title,5) }}</a>
                        </h2>
                        <div class="post-meta">
                            <span class="post-date">{{ $latestposts[$i]->created_at->toFormattedDateString() }}</span>
                        </div>
                        </div><!-- Post content end -->
                    </div><!-- Post block style end -->
                    </li><!-- Li 1 end -->
                @endfor
                

                </ul><!-- List post end -->
            </div><!-- List post block end -->

        </div><!-- Popular news widget end --> --}}

    </div><!-- Sidebar right end -->
</div><!-- Sidebar Col end -->

@push('script')
  <script>
    $('#subscribe').submit(function (e) { 
      e.preventDefault();
      var form = new FormData(this);
      $.ajax({
        url:"{{ route('subscribe') }}",
        type:"POST",
        data: form,
        dataType:"Json",
        cache: false,
        contentType: false,
        processData: false
      })  
      .done(function (res) {
        console.log(res);
        if(res.success){
          $('#subres').text(res.message).css('color','green');
    
        }
        else{
          $('#subres').text(res.message).css('color','red');
        }
        
      })
    });
  </script>
@endpush