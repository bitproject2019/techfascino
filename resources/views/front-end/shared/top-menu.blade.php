<div class="site-nav-inner float-left">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
       <span class="fa fa-bars"></span>
    </button>
    <!-- End of Navbar toggler -->

    <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse clearfix">
        <ul class="nav navbar-nav">
            <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn"   href="{{ route('index') }}" >
                    <span class="tab-head">
                        <span class="tab-text-title">Home</span>					
                    </span>
                </a>
            </li>

            <li class="nav-item {{ (request()->is('technology*') || request()->is('Technology*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn"  href="{{ route('category','technology') }}" >
                    <span class="tab-head">
                        <span class="tab-text-title">Technology</span>					
                    </span>
                </a>
            </li>

            <li class="nav-item {{ (request()->is('mobile*') || request()->is('Mobile*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','mobile') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">Mobile</span>					
                    </span>
                </a>
            </li>

            <li class="nav-item {{ (request()->is('gadget*') || request()->is('Gadget*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','gadget') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">Gadget</span>					
                    </span>
                </a>
            </li>

            <li class="nav-item {{ (request()->is('science*') || request()->is('Science*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','science') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">Science</span>					
                    </span>
                </a>
            </li>
            
            <li class="nav-item {{ (request()->is('world*') || request()->is('World*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','world') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">World</span>					
                    </span>
                </a>
            </li>
                
            <li class="nav-item {{ (request()->is('lifestyle*') ||request()->is('Lifestyle*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','lifestyle') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">Lifestyle</span>					
                    </span>
                </a>
            </li>
            <li class="nav-item {{ (request()->is('video*') || request()->is('Video*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn" href="{{ route('category','video') }}">
                    <span class="tab-head">
                        <span class="tab-text-title">Video</span>					
                    </span>
                </a>
            </li>
            <li class="nav-item {{ (request()->is('contact*') || request()->is('Contact*')) ? 'active' : '' }}">
                <a class="nav-link animated fadeIn "  href="{{ route('contact') }}" >
                    <span class="tab-head">
                        <span class="tab-text-title">Contact</span>					
                    </span>
                </a>
            </li> 
           
        </ul><!--/ Nav ul end -->
    </div><!--/ Collapse end -->

</div><!-- Site Navbar inner end -->