<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-12 text-md-center">
      <div class="logo">
         <a href="{{ route('index' )}}">
          <img src="/front-end/images/logos/logo.png" alt="">
         </a>
      </div>
    </div><!-- logo col end -->

    <div class="col-lg-9 col-md-12 header-right">
      <div class="ad-banner float-right">
        <a href="#"><img src="/front-end/images/banner-ads/ad-top-header.png" class="img-fluid" alt="" width="680px" height="50px"></a>
      </div>
    </div><!-- header right end -->
  </div><!-- Row end -->
</div><!-- Logo and banner area end -->