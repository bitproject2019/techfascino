<div class="footer-info">
  <div class="container">
     <div class="row">
        <div class="col-lg-3 col-md-12">
           <div class="footer-info-content">
              <div class="footer-logo">
                 <img class="img-fluid" src="/front-end/images/logos/footer-logo.png" alt="" />
              </div>
           </div>
        </div>
        <div class="col-lg-5 col-md-12">
           <div class="footer-info-content">
              <p>Tech Fascino Worldwide is a popular online newsportal and going source for technical and digital content for its influential audience around the globe. You can get to know the latest news about the all over the world espically about the tech news.</p>
           </div>
        </div>
        <div class="col-lg-4 col-md-12">
           <div class="footer-info-content">
              <p class="footer-info-phone"><i class="fa fa-phone"></i> +(9477) 128-9586</p>
              <p class="footer-info-email"><i class="fa fa-envelope-o"></i> techfascino@gmail.com</p>
              <ul class="unstyled footer-social">
                 <li>
                    <a title="Rss" href="#">
                       <span class="social-icon"><i class="fa fa-rss"></i></span>
                    </a>
                    <a title="Facebook" href="https://www.facebook.com/techfascino/">
                       <span class="social-icon"><i class="fa fa-facebook"></i></span>
                    </a>
                    <a title="Twitter" href="#">
                       <span class="social-icon"><i class="fa fa-twitter"></i></span>
                    </a>
                    <a title="Google+" href="#">
                       <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                    </a>
                    <a title="Linkdin" href="#">
                       <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                    </a>
                    <a title="Skype" href="#">
                       <span class="social-icon"><i class="fa fa-skype"></i></span>
                    </a>
                    <a title="Skype" href="#">
                       <span class="social-icon"><i class="fa fa-dribbble"></i></span>
                    </a>
                    <a title="Skype" href="#">
                       <span class="social-icon"><i class="fa fa-pinterest"></i></span>
                    </a>
                 </li>
              </ul>
           </div><!-- Footer info content end -->
        </div><!-- Col end -->
     </div><!-- Row end -->
  </div><!-- Container end -->
</div><!-- Footer info end -->
<div class="footer-main">
<div class="container">
 <div class="row">
   <div class="col-lg-3 col-sm-12 footer-widget">
     <h3 class="widget-title">Trending Now</h3>
     <div class="list-post-block">
       <ul class="list-post">
          @for ($i = 0; $i < 3; $i++)
            <li class="clearfix">
            <div class="post-block-style post-float clearfix">
               <div class="post-thumb">
                  <a href="{{ route('singlepost',[$randomposts[$i]->categories[0]->slug ,$randomposts[$i]->slug]) }}">
                  <img class="img-fluid" src="{{ isset($randomposts[$i]->image)?'/post_image/'.$randomposts[$i]->image:'/category_image/'.$randomposts[$i]->categories[0]->image }}" alt="" />
                  </a>
               </div><!-- Post thumb end -->

               <div class="post-content">
                  <h2 class="post-title title-small">
                     <a href="{{ route('singlepost',[$randomposts[$i]->categories[0]->slug ,$randomposts[$i]->slug]) }}">{{ \Illuminate\Support\Str::words($randomposts[$i]->title,3) }}</a>
                  </h2>
                  <div class="post-meta">
                     <span class="post-date">{{ $randomposts[$i]->created_at->toFormattedDateString() }}</span>
                  </div>
               </div><!-- Post content end -->
            </div><!-- Post block style end -->
            </li><!-- Li 1 end -->
         @endfor
        
       </ul><!-- List post end -->
     </div><!-- List post block end -->
     
   </div><!-- Col end -->

   <div class="col-lg-3 col-sm-12 footer-widget widget-categories">
     <h3 class="widget-title">Hot Categories</h3>
     <ul>
        @foreach ($categories as $category)  
         <li>
            <a href="{{ route('category',$category->slug) }}"><span class="catTitle">{{ $category->name }}</span><span class="catCounter"> ({{ $category->posts->count() }})</span></a>
         </li>
        @endforeach
      
     </ul>
     
   </div><!-- Col end -->

   <div class="col-lg-3 col-sm-12 footer-widget">
           <h3 class="widget-title">Latest Posts</h3>
           <div class="list-post-block">
              <ul class="list-post">
                 @foreach ($latestposts as $latestpost)      
                  <li class="clearfix">
                     <div class="post-block-style post-float clearfix">
                        <div class="post-thumb">
                           <a href="{{ route('singlepost',[$latestpost->categories[0]->slug ,$latestpost->slug]) }}">
                              <img class="img-fluid" src="{{ isset($latestpost->image)?'/post_image/'.$latestpost->image:'/category_image/'.$latestpost->categories[0]->image }}" alt="" />
                           </a>
                        </div><!-- Post thumb end -->

                        <div class="post-content">
                              <h2 class="post-title title-small">
                                 <a href="{{ route('singlepost',[$latestpost->categories[0]->slug ,$latestpost->slug]) }}">{{ \Illuminate\Support\Str::words($latestpost->title,3) }}</a>
                              </h2>
                              <div class="post-meta">
                                 <span class="post-date">{{ $latestpost->created_at->toFormattedDateString() }}</span>
                              </div>
                           </div><!-- Post content end -->
                     </div><!-- Post block style end -->
                  </li><!-- Li 1 end -->
                 @endforeach

                 
              </ul><!-- List post end -->
           </div><!-- List post block end -->
              
        </div><!-- Col end -->

   <div class="col-lg-3 col-sm-12 footer-widget widget-tags">
           <h3 class="widget-title">Tags</h3>
           <ul class="unstyled clearfix">
              <li><a href="#">Apps<span class="tag-count badge badge-info">13</span></a></li>
              <li><a href="#">Architechture<span class="tag-count badge badge-info">2</span></a></li>
              <li><a href="#">Food<span class="tag-count badge badge-info">9</span></a></li>
              <li><a href="#">Gadgets<span class="tag-count badge badge-info">15</span></a></li>
              <li><a href="#">Games<span class="tag-count badge badge-info">5</span></a></li>
              <li><a href="#">Health<span class="tag-count badge badge-info">10</span></a></li>
              <li><a href="#">Lifestyles<span class="tag-count badge badge-info">11</span></a></li>
              <li><a href="#">Robotics<span class="tag-count badge badge-info">20</span></a></li>
              <li><a href="#">Software<span class="tag-count badge badge-info">56</span></a></li>
              <li><a href="#">Tech<span class="tag-count badge badge-info">4</span></a></li>
              <li><a href="#">Travel<span class="tag-count badge badge-info">19</span></a></li>
              <li><a href="#">Video<span class="tag-count badge badge-info">13</span></a></li>
           </ul>
   </div><!-- Col end -->

 </div><!-- Row end -->
</div><!-- Container end -->
</div><!-- Footer main end -->