<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
  <meta charset="utf-8">
  <meta name="csrf-token"  content="{{ csrf_token() }}">
  <meta name="description" content="Tech Fascino - A revolution in the tech world">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Tech Fascino - @yield('title','Home') </title>

	<!-- Mobile Specific Metas
	================================================== -->

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

	<!--Favicon-->
	<link rel="shortcut icon" href="/front-end/images/favicon.html" type="image/x-icon">
	<link rel="icon" href="/back-end/logo/icon.png" type="image/x-icon">
	
	<!-- CSS
	================================================== -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="/front-end/css/bootstrap.min.css">
	<!-- FontAwesome -->
   <link rel="stylesheet" href="/front-end/css/font-awesome.min.css">
   <!-- Animate CSS -->
   <link rel="stylesheet" href="/front-end/css/animate.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="/front-end/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/front-end/css/owl.theme.default.min.css">
	<!-- Colorbox -->
	<link rel="stylesheet" href="/front-end/css/colorbox.css">
	<!-- Template styles-->
	<link rel="stylesheet" href="/front-end/css/style.css">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="/front-end/css/responsive.css">

</head>
	
<body>

	<div class="body-inner">

   <div class="trending-bar d-md-block d-lg-block d-none">
      <div class="container">
         <div class="row">
           @include('front-end.shared.trending')
         </div><!--/ Row end -->
      </div><!--/ Container end -->
   </div><!--/ Trending end -->

	<div id="top-bar" class="top-bar">
    @include('front-end.shared.top-bar')
	</div><!--/ Topbar end -->

	<!-- Header start -->
	<header id="header" class="header">
    @include('front-end.shared.header')
	</header><!--/ Header end -->

	<div class="main-nav clearfix dark-nav">
		<div class="container">
			<div class="row">
				<nav class="navbar navbar-expand-lg col">
            		@include('front-end.shared.top-menu')
				</nav><!--/ Navigation end -->

				<div class="nav-search">
					<span id="search"><i class="fa fa-search"></i></span>
				</div><!-- Search end -->
				
				<div class="search-block" style="display: none;">
					<form action="{{ route('searchresults') }}" method="get">
						<input type="text" class="form-control" placeholder="Type what you want and enter" name="search">
					</form>
					<span class="search-close">&times;</span>
				</div><!-- Site search end -->

			</div><!--/ Row end -->
		</div><!--/ Container end -->

	</div><!-- Menu wrapper end -->

	  @yield('content')

	<footer id="footer" class="footer">
    @include('front-end.shared.footer') 

	</footer><!-- Footer end -->

	<div class="copyright">
    @include('front-end.shared.copyright') 
   </div><!-- Copyright end -->


	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script type="text/javascript" src="/front-end/js/jquery.js"></script>
	<!-- Popper Jquery -->
	<script type="text/javascript" src="/front-end/js/popper.min.js"></script>
	<!-- Bootstrap jQuery -->
	<script type="text/javascript" src="/front-end/js/bootstrap.min.js"></script>
	<!-- Owl Carousel -->
	<script type="text/javascript" src="/front-end/js/owl.carousel.min.js"></script>
	<!-- Color box -->
	<script type="text/javascript" src="/front-end/js/jquery.colorbox.js"></script>
	<!-- Smoothscroll -->
	<script type="text/javascript" src="/front-end/js/smoothscroll.js"></script>
	<!-- Template custom -->
	<script type="text/javascript" src="/front-end/js/custom.js"></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools --> 
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cee4c071f0f4e4b"></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5cee4c071f0f4e4b"></script>
  @stack('script')
	</div><!-- Body inner end -->
</body>
</html>