<!DOCTYPE HTML>

<html lang="en"> 

<head>
<meta charset="utf-8">
<title>Tech Fascino - Coming Soon</title>
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold"> 
<link rel="stylesheet" type="text/css" href="coming-soon/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="coming-soon/css/styles.css">
</head>

<body id="home">
<section class="main">
<div id="Content" class="wrapper topSection">
	<div id="Header">
	<div class="wrapper">
		<div class="logo"><h1><img src="coming-soon/images/logo.png" />TECH FASCINO</h1>	</div>
		</div>
	</div>
	<h2>We are coming soon!!!</h2> 	
<div class="countdown styled"></div>
</div>
</section>
<section class="subscribe spacing">
<div class="container">
<div id="subscribe">
	
	<div id="socialIcons">
		<ul> 
			<li><a href="#" title="Twitter" class="twitterIcon"></a></li>
			<li><a href="https://www.facebook.com/techfascino/" title="facebook" class="facebookIcon"></a></li>
			<li><a href="#" title="linkedIn" class="linkedInIcon"></a></li>
			<li><a href="#" title="Pintrest" class="pintrestIcon"></a></li>
		</ul>
	</div>
	</div>
</div>
</section>

<!--Scripts-->
<script type="text/javascript" src="coming-soon/js/jquery-1.9.1.min.js"></script> 
<script type="text/javascript" src="coming-soon/js/jquery.countdown.js"></script>
<script type="text/javascript" src="coming-soon/js/global.js"></script>

</body>

</html>
