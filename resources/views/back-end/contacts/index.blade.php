@extends('back-end.template')
@section('title','List of Contact Messages')
@section('content')

<div class="page-header">
    <h3 class="page-title">
        List of Contact Messages
    </h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Contact</a></li>
        <li class="breadcrumb-item active" aria-current="page">All Contact Messages</li>
        </ol>
    </nav>
    </div>
    <div class="row">  
        <div class="col-md-12">
            <div class="card card-default card-demo" id="cardChart9">
                <div class="card-header">
                    <div class="float-right">
                       
                    </div>
                    <div class="card-title">List of Contact Messages</div>
                </div>
                <div class="card-wrapper collapse show">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table display nowrap" datatable style="font-size:12px" id="contactstable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Comments</th>
                                    <th>Status</th>
                                    @can('edit_contacts')
                                        <th>Action</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td>{{$contact->name}}</td>
                                        <td>{{$contact->email}}</td>
                                        <td>{{ $contact->subject}}</td>
                                        <td>{{ $contact->message}}</td>
                                        <td>{{ ($contact->status==0)?'Unread':'Read'}}</td>
                                        @can('edit_contacts')
                                        <td>
                                            <a href="{{route('contacts.edit',$contact->id)}}" class="btn btn-sm btn-info ajax-load-modal" data-title="Contact Status"><i class="fa fa-pencil-alt"></i></a>
                                            <a href="{{route('contacts.destroy',$contact->id)}}" class="btn btn-sm btn-warning ajax-delete" data-set="tr"><i class="fa fa-trash"></i></a>
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@push('script')                    
    <script>

        $(document).ready(function () {
            $('#contactstable').DataTable({
            });
        })

    </script>
@endpush
@stop

