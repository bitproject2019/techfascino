
<div class="form-group">
    {!!Form::label('title', 'Title')!!}
    {!!Form::text('title',null,['class' =>'form-control'])!!}
</div>

<div class="form-group">
    {!!Form::label('slug', 'Slug')!!}
    {!!Form::text('slug',null,['class' =>'form-control'])!!}
</div>

<div class="form-group">
    {!!Form::label('content', 'Content')!!}
    {!!Form::textarea('content',null,['class' =>'form-control','id'=>'summernoteExample' ])!!}
    {{-- ,'id'=>'summernoteExample' --}}
</div>

<div class="form-group">
    {!!Form::label('video', 'Video Link')!!}
    {!!Form::textarea('video',null,['class' =>'form-control' ])!!}
    {{-- ,'id'=>'summernoteExample' --}}
</div>

<div class="form-group">
    {!!Form::label('image', 'Featured Image')!!}
    {!! Form::file('image',['class'=>'dropify','data-default-file'=>isset($post->image)?url('post_image/'.$post->image):null])!!}
</div>
