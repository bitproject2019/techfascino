
<div class="form-group">
    {!!Form::label('name', 'Name')!!}
    {!!Form::text('name',null,['class' =>'form-control'])!!}
</div>

{{-- <div class="form-group">
    {!!Form::label('slug', 'Slug')!!}
    {!!Form::text('slug',null,['class' =>'form-control'])!!}
</div> --}}

<div class="form-group">
    {!!Form::label('parent', 'Parent Category')!!}
    {!!Form::select('parent',$categories,null,['class' =>'form-control col-12','placeholder'=>'Choose'])!!}
</div>

<div class="form-group">
    {!!Form::label('image', 'Category Image')!!}
    {!!Form::file('image',['class' =>'dropify','data-default-file'=>isset($category->image)? url('category_image/' . $category->image):null])!!}
    {{-- // 'data-default-file'=>isset($service->image)? url('serviceimages/' . $service->image):null) --}}
</div>
    