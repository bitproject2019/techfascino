<?php

return [

    'telegram' => [
        'api_token' => '',
        'bot_username' => '',
        'channel_username' => '', // Channel username to send message
        'channel_signature' => '', // This will be assigned in the footer of message
        'proxy' => false,   // True => Proxy is On | False => Proxy Off
    ],

    'twitter' => [
        'consurmer_key' => '',
        'consurmer_secret' => '',
        'access_token' => '',
        'access_token_secret' => ''
    ],

    'facebook' => [
        'app_id' => '807111276144422',
        'app_secret' => 'dedc61ef4cd9b4053c2a1cf0d018ef57',
        'default_graph_version' => 'v3.3',
        'page_access_token' => 'EAAXe52J9wC0BAFTXyUVf8jp6gLwzM7EZAB7xnL1JDbvj7cJF8VlCJ0ODRDCcba8oQbtItyt2QPWxoeCXWjdXs44MF6ZBL040KfazROYWcoYY8yXcnPSZBMYeiyjdNOHi5SqHvggfuQlSua8o9dyQkoHon0yTytnRhD0boEZADd9ZAeETlr5jtqh5ZCGEO3bQkGl9PnnJITaGJB3Bqkd620'
    ],

    // Set Proxy for Servers that can not Access Social Networks due to Sanctions or ...
    'proxy' => [
        'type' => '',   // 7 for Socks5
        'hostname' => '', // localhost
        'port' => '' , // 9050
        'username' => '', // Optional
        'password' => '', // Optional
    ]
];
